﻿
namespace Appxplore.APCP
{
    public class APCPDefine 
    {
        public enum AnimationType
        {
            FromBottom = 0,
            FromTop = 1,
            FromLeft = 2,
            FromRight = 3
        }
    }

    [System.Serializable]
    public class InterstitialData
    {
        public string Location;
        public string LocationKey;
    }
}
