﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace Appxplore.APCP
{
    #if UNITY_EDITOR
    using UnityEditor;
    [InitializeOnLoad]
    #endif
    public class APCPSetting : ScriptableObject
    {
        const string settingAssetsName = "apcpSettings";
        const string settingPath = "APCP/Runtime/Resources";
        const string settingExtension = ".asset";
        public const string VERSION = "1.3.2";

        private static APCPSetting instance;

        static APCPSetting Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = Resources.Load(settingAssetsName) as APCPSetting;
                    if (instance == null)
                    {
                        // If not found, autocreate the asset object.
                        instance = CreateInstance<APCPSetting>();

                        #if UNITY_EDITOR
                        string properPath = Path.Combine(Application.dataPath, settingPath);
                        if (!Directory.Exists(properPath))
                        {
                            AssetDatabase.CreateFolder("Packages/APCP/Runtime", "Resources");
                        }

                        string fullPath = Path.Combine(Path.Combine("Packages", settingPath),
                            settingAssetsName + settingExtension
                        );
                        AssetDatabase.CreateAsset(instance, fullPath);
                        #endif
                    }
                }
                return instance;
            }
        }

        #if UNITY_EDITOR
        [MenuItem("AppxplorePlugin/Edit APCP Settings")]
        public static void Edit()
        {
            Selection.activeObject = Instance;
        }
        #endif

        [SerializeField]
        protected bool isLoggingEnabled = false;

        [SerializeField]
        protected string gameId = string.Empty;

        [SerializeField]
        protected string urlScheme = string.Empty;

        [SerializeField]
        protected bool isAutoCacheMoreGame = true;

        [SerializeField]
        protected bool isCacheMoreGameOnStartOnly = true;

        [SerializeField]
        protected bool isAutoCacheInterstitial = true;

        [SerializeField]
        protected APCPDefine.AnimationType moreGameAnimationType = APCPDefine.AnimationType.FromBottom;

        [SerializeField]
        protected APCPDefine.AnimationType interstitialAnimationType = APCPDefine.AnimationType.FromBottom;

        [SerializeField]
        public List<InterstitialData> Interstitials;

        #if UNITY_EDITOR
        public void enableLogging(bool enabled)
        {
            if (Instance.isLoggingEnabled != enabled)
            {
                Instance.isLoggingEnabled = enabled;
                DirtyEditor ();
            }
        }

        public void enableAutoCacheMoreGame(bool enabled)
        {
            if (Instance.isAutoCacheMoreGame != enabled)
            {
                Instance.isAutoCacheMoreGame = enabled;
                DirtyEditor ();
            }
        }

        public void enableAutoCacheMoreGameOnStart(bool enabled)
        {
            if (Instance.isCacheMoreGameOnStartOnly != enabled)
            {
                Instance.isCacheMoreGameOnStartOnly = enabled;
                DirtyEditor ();
            }
        }

        public void enableAutoCacheInterstitial(bool enabled)
        {
            if (Instance.isAutoCacheInterstitial != enabled)
            {
                Instance.isAutoCacheInterstitial = enabled;
                DirtyEditor ();
            }
        }

        public void setGameId(string _gameId)
        {
            if (Instance.gameId != _gameId)
            {
                Instance.gameId = _gameId;
                DirtyEditor ();
            }
        }

        public void setURLScheme(string _scheme)
        {
            if (Instance.urlScheme != _scheme)
            {
                Instance.urlScheme = _scheme;
                DirtyEditor();
            }
        }

        public void setMoreGameAnimationType(APCPDefine.AnimationType _type)
        {
            if (Instance.moreGameAnimationType != _type)
            {
                Instance.moreGameAnimationType = _type;
                DirtyEditor();
            }
        }

        public void setInterstitialAnimationType(APCPDefine.AnimationType _type)
        {
            if (Instance.interstitialAnimationType != _type)
            {
                Instance.interstitialAnimationType = _type;
                DirtyEditor();
            }
        }

        private static void DirtyEditor()
        {
            EditorUtility.SetDirty(Instance);
        }


        #endif

        public static void setDebugMode(bool enabled)
        {
            if (Instance.isLoggingEnabled != enabled)
            {
                Instance.isLoggingEnabled = enabled;
            }
        }

        public static void setAutoCacheMoreGame(bool enabled)
        {
            if (Instance.isAutoCacheMoreGame != enabled)
            {
                Instance.isAutoCacheMoreGame = enabled;
            }
        }

        public static void setAutoCacheInterstitial(bool enabled)
        {
            if (Instance.isAutoCacheInterstitial != enabled)
            {
                Instance.isAutoCacheInterstitial = enabled;
            }
        }


        public static bool isLogging
        {
            get{
                return Instance.isLoggingEnabled;
            }
        }

        public static bool isAutoCachedInterstitial
        {
            get{
                return Instance.isAutoCacheInterstitial;
            }
        }

        public static bool isAutoCachedMoreGame
        {
            get{
                return Instance.isAutoCacheMoreGame;
            }
        }

        public static bool isAutoCachedMoreGameOnStart
        {
            get{
                return Instance.isCacheMoreGameOnStartOnly;
            }
        }

        public static string GameId
        {
            get{
                return Instance.gameId;
            }
        }

        public static string UrlScheme
        {
            get{
                return Instance.urlScheme;
            }
        }

        public static APCPDefine.AnimationType MoreGameAnimationType
        {
            get{
                return Instance.moreGameAnimationType;
            }
        }

        public static APCPDefine.AnimationType InterstitialAnimationType
        {
            get{
                return Instance.interstitialAnimationType;
            }
        }

        public static string[] InterstitialsLocationList
        {
            get{
                string[] list = new string[Instance.Interstitials.Count];
                for (int i = 0; i < Instance.Interstitials.Count; i++)
                    list[i] = Instance.Interstitials[i].LocationKey;
                return list;
            }
        }

        public static string getLocationKeyFromName(string _name)
        {
            InterstitialData temp = Instance.Interstitials.Find(ni => ni.Location == _name);

            if (temp != null)
                return temp.LocationKey;
            
            Debug.LogWarning("Location Name is not define: " + _name);
            return string.Empty;
        }
    }
}


