﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Appxplore.APCP.Common;
using Appxplore.APCP.Platforms;

namespace Appxplore.APCP
{
    public class APCPAgent
    {
        private static APCPAgent mInstance = null;

        public static void createInstanceIfNeeded()
        {
            if (mInstance == null)
                mInstance = new APCPAgent();
        }

        public static APCPAgent Instance
        {
            get{
                createInstanceIfNeeded();
                return mInstance;
            }
        }

        IAPCPClient m_client;

        protected Action<bool> m_initCallback;
        protected Action m_moreGameCallback;
        protected Action m_interstitialCallback;
        protected Action<bool> m_consentCallback;


        public APCPAgent()
        {
            m_client = APCPClientFactory.buildAPCPClient();
            m_client.OnInitDone += OnInitDone;
            m_client.OnInitFailed += OnInitFail;
            m_client.OnMoreGameClosed += OnMoreGameBack;
            m_client.OnInterstitialClosed += OnInterstitialClosed;
            m_client.OnConsentReturned += OnConsentReturn;
        }

        public void initClient()
        {
            //APCPSetting.setDebugMode(_isDebug);
            m_client.setMoreGameCacheOnStartOnly(APCPSetting.isAutoCachedMoreGameOnStart);
            m_client.setInterstitialLocationList(APCPSetting.InterstitialsLocationList);

            m_client.initialize(APCPSetting.GameId, APCPSetting.isLogging);
            m_client.setMoreGameAnimationType(APCPSetting.MoreGameAnimationType);
            m_client.setInterstitialAnimationType(APCPSetting.InterstitialAnimationType);
        }

        public void connect(Action<bool> _callback)
        {
            m_initCallback = _callback;
            m_client.connect();
        }

        public void setCustomUserId(string _userId)
        {
            m_client.setCustomUserId(_userId);
        }

        public void setCustomLanguage(string _customLang)
        {
            m_client.setCustomLanguage(_customLang);
        }

        public void setAutoCacheMoreGame(bool _autoCache)
        {
            APCPSetting.setAutoCacheMoreGame(_autoCache);
            m_client.setMoreGameAutoCache(_autoCache);
        }

        public void setMoreGameAnimationType(APCPDefine.AnimationType _type)
        {
            m_client.setMoreGameAnimationType(_type);
        }

        public bool isMoreGameReady()
        {
            return m_client.isMoreGameCached();
        }

        public void cacheMoreGame()
        {
            m_client.cacheMoreGames();
        }

        public bool showMoreGame(Action _callback)
        {
            if (m_client.showMoreGames())
            {
                m_moreGameCallback = _callback;
                return true;
            }

            return false;
        }

        public void setAutoCacheInterstitial(bool _autoCache)
        {
            APCPSetting.setAutoCacheInterstitial(_autoCache);
            m_client.setInterstitialAutoCache(_autoCache);
        }

        public void setInterstitialAnimationType(APCPDefine.AnimationType _type)
        {
            m_client.setInterstitialAnimationType(_type);
        }

        public bool isInterstitialReady(string _locationName)
        {
            return m_client.isInterstitialCached(APCPSetting.getLocationKeyFromName(_locationName));
        }

        public void cacheInterstitial(string _locationName)
        {
            m_client.cacheInterstitial(APCPSetting.getLocationKeyFromName(_locationName));
        }

        public void cacheAllInterstitials()
        {
            m_client.cacheAllInterstitials();
        }

        public bool showInterstitial(string _locationName, Action _callback)
        {
            if (m_client.showInterstitial(APCPSetting.getLocationKeyFromName(_locationName)))
            {
                m_interstitialCallback = _callback;
                return true;
            }

            return false;
        }

        public string getDeepLinkData()
        {
            return m_client.getDeepLinkData();
        }

        public void removeDeepLinkData()
        {
            m_client.removeDeepLinkData();
        }

        public bool isConsentGiven()
        {
            return m_client.isConsentGiven();
        }

        public void setConsent(bool _consent)
        {
            m_client.setConsent(_consent);
        }

        public void showConsent(string _privacyPolicyUrl, Action<bool> _callback)
        {
            m_consentCallback = _callback;
            m_client.showConsentDialog(_privacyPolicyUrl);
        }

        void OnInitDone()
        {
            if (m_initCallback != null)
            {
                m_initCallback(true);
                m_initCallback = null;
            }
        }

        void OnInitFail(string _error)
        {
            if (m_initCallback != null)
            {
                m_initCallback(false);
                m_initCallback = null;
            }
        }

        void OnMoreGameBack()
        {
            if (m_moreGameCallback != null)
            {
                m_moreGameCallback();
                m_moreGameCallback = null;
            }
        }

        void OnInterstitialClosed()
        {
            if (m_interstitialCallback != null)
            {
                m_interstitialCallback();
                m_interstitialCallback = null;
            }
        }

        void OnConsentReturn(bool _result)
        {
            if (m_consentCallback != null)
            {
                m_consentCallback(_result);
                m_consentCallback = null;
            }
        }
    }
}
