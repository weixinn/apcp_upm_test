﻿using System;
using System.Collections;
using UnityEngine;

using Appxplore.APCP.Common;

namespace Appxplore.APCP.Android
{
    #if UNITY_ANDROID
    public class APCPClient :  AndroidJavaProxy, IAPCPClient
    {
        public event Action OnInitDone;
        public event Action<string> OnInitFailed;
        public event Action OnMoreGameClosed;
        public event Action OnInterstitialClosed;
        public event Action<bool> OnConsentReturned;

        private bool isDebugLog;

        private AndroidJavaObject apcpObject;

        public APCPClient() :base(APCPUtils.APCPListenerClassName)
        {
            apcpObject = new AndroidJavaObject(APCPUtils.APCPClassName);
            isDebugLog = false;
        }

        #region implementation of Method to Android
        public void initialize(string gameId, bool isDebug)
        {
            isDebugLog = isDebug;

            AndroidJavaClass playerClass = new AndroidJavaClass(APCPUtils.UnityActivityClassName);
            AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");

            //in order to save cost of bandwidth
            apcpObject.CallStatic("setScreenOrientation", Screen.autorotateToPortrait || Screen.autorotateToPortraitUpsideDown, Screen.autorotateToLandscapeLeft || Screen.autorotateToLandscapeRight);
            apcpObject.CallStatic("initialize", activity, gameId, this, isDebug);
        }

        public void connect()
        {
            apcpObject.CallStatic("connect");
        }

        public void setCustomUserId(string _userId)
        {
            apcpObject.CallStatic("setCustomId", _userId);
        }

        public void setCustomLanguage(string _langId)
        {
            apcpObject.CallStatic("setCustomLang", _langId);
        }

        //MoreGame
        public void setMoreGameCacheOnStartOnly(bool _startOnly)
        {
            apcpObject.CallStatic("setAutoCacheMoreGameOnStartOnly", _startOnly);
        }

        public void setMoreGameAutoCache(bool _auto)
        {
            apcpObject.CallStatic("setAutoCacheMoreGame", _auto);
        }

        public void setMoreGameAnimationType(APCPDefine.AnimationType _type)
        {
            apcpObject.CallStatic("setMoreGameAnimationType", (int)_type);
        }

        public bool isMoreGameCached()
        {
            return apcpObject.CallStatic<bool>("isMoreGameReady");
        }

        public void cacheMoreGames()
        {
            apcpObject.CallStatic("cacheMoreGames");
        }

        public bool showMoreGames()
        {
            return apcpObject.CallStatic<bool>("showMoreGames");
        }

        //Interstitial
        public void setInterstitialAutoCache(bool _auto)
        {
            apcpObject.CallStatic("setAutoCacheInterstitial", _auto);
        }

        public void setInterstitialLocationList(string[] locations)
        {
            if (locations.Length == 0)
            {
                Debug.LogError("Please set at least 1 location for the interstitials");
            }
            apcpObject.CallStatic("setInterstitialLocationList", javaArrayFromCS(locations));
        }

        public void setInterstitialAnimationType(APCPDefine.AnimationType _type)
        {
            apcpObject.CallStatic("setInterstitialAnimationType", (int)_type);
        }

        public bool isInterstitialCached(string _location)
        {
            return apcpObject.CallStatic<bool>("isInterstitialReady", _location);
        }

        public void cacheInterstitial(string _location)
        {
            apcpObject.CallStatic("cacheInterstitial", _location);
        }

        public void cacheAllInterstitials()
        {
            apcpObject.CallStatic("cacheAllInterstitials");
        }

        public bool showInterstitial(string _location)
        {
            return apcpObject.CallStatic<bool>("showInterstitial", _location);
        }

        public void removeDeepLinkData()
        {
            apcpObject.CallStatic("removeDeepLinkData");
        }

        public string getDeepLinkData()
        {
            return apcpObject.CallStatic<string>("getDeepLinkData");
        }

        public void setConsent(bool _consentGiven)
        {
            apcpObject.CallStatic("setConsent", _consentGiven);
        }

        public bool isConsentGiven()
        {
            return apcpObject.CallStatic<bool>("isConsentGiven");
        }

        public void showConsentDialog(string _privacyPolicyUrl)
        {
            apcpObject.CallStatic("showConsentDialog", _privacyPolicyUrl);
        }

        #endregion

        #region implementation of Listener from Android
        public void onInitDone()
        {
            if (isDebugLog)
                Debug.Log("OnInitDone");

            if (OnInitDone != null)
                OnInitDone();
        }

        void onInitFailed(String message)
        {
            if (isDebugLog)
                Debug.Log("onInitFailed: " + message);

            if (OnInitFailed != null)
                OnInitFailed(message);
        }

        void onMoreGamesClosed()
        {
            if (isDebugLog)
                Debug.Log("onMoreGamesClosed");

            if (OnMoreGameClosed != null)
                OnMoreGameClosed();
        }

        void onMoreGamesCachedReady()
        {
            if (isDebugLog)
                Debug.Log("onMoreGamesCachedReady");
        }

        void onMoreGamesCachedError(String message)
        {
            if (isDebugLog)
                Debug.Log("onMoreGamesCachedError: " + message);
        }

        void onInterstitialClosed(string _location)
        {
            if (isDebugLog)
                Debug.Log("onInterstitialClosed: " + _location);
            
            if (OnInterstitialClosed != null)
                OnInterstitialClosed();
        }

        void onInterstitialCachedReady(string _location)
        {
            if (isDebugLog)
                Debug.Log("onInterstitialCachedReady: " + _location );
        }

        void onInterstitialCachedError(string _location, string message)
        {
            if (isDebugLog)
                Debug.Log("onInterstitialCachedError=> Location: " + _location +", message: " + message);
        }

        void onConsentDialogResult(bool consentGiven)
        {
            if (isDebugLog)
                Debug.Log("onConsentDialogResult=> consentGiven: " + consentGiven);

            if (OnConsentReturned != null)
                OnConsentReturned(consentGiven);
        }

        #endregion

        #region Android Java Reflection
        private AndroidJavaObject javaArrayFromCS(string [] values) 
        {
            AndroidJavaClass arrayClass  = new AndroidJavaClass("java.lang.reflect.Array");
            AndroidJavaObject arrayObject = arrayClass.CallStatic<AndroidJavaObject>("newInstance",
                new AndroidJavaClass("java.lang.String"),
                values.Length);
            for (int i=0; i<values.Length; ++i) 
            {
                arrayClass.CallStatic("set", arrayObject, i,
                    new AndroidJavaObject("java.lang.String", values[i]));
            }

            return arrayObject;
        }
        #endregion
    }

    #endif
}
