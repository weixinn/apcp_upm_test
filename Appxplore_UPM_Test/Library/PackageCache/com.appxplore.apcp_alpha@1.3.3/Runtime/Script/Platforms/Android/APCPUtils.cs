﻿using System.Collections;

namespace Appxplore.APCP.Android
{
    #if UNITY_ANDROID
    public class APCPUtils
    {
        public const string APCPClassName = "com.appxplore.apcp.APCPromo";
        public const string APCPListenerClassName = "com.appxplore.apcp.IAPCPromoListener";
        public const string UnityActivityClassName = "com.unity3d.player.UnityPlayer";
    }
    #endif
}
