﻿using System;
using System.Collections;
using System.Reflection;

using UnityEngine;

using Appxplore.APCP.Common;

namespace Appxplore.APCP.Platforms
{
    #if UNITY_EDITOR || UNITY_STANDALONE
    public class DummyClient : IAPCPClient
    {

        public event Action OnInitDone;
        public event Action<string> OnInitFailed;
        public event Action OnMoreGameClosed;
        public event Action OnInterstitialClosed;
        public event Action<bool> OnConsentReturned;

        private bool isDebugLog;

        //class creation
        public DummyClient()
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }
            
        //Init Section
        public void initialize(string gameId)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
            OnInitDone();
        }

        public void initialize(string gameId, bool isDebug)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
            OnInitFailed("Fail!");
        }

        public void connect()
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
            OnInitDone();
        }

        public void setCustomUserId(string userId)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }

        public void setCustomLanguage(string langId)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }


        //More Game Section

        public void setMoreGameCacheOnStartOnly(bool _startOnly)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }

        public void setMoreGameAutoCache(bool _auto)
        {       
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }

        public void setMoreGameAnimationType(APCPDefine.AnimationType _type)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name + ": " + _type);
        }

        public bool isMoreGameCached()
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
            return false;
        }

        public void cacheMoreGames()
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }

        public bool showMoreGames()
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
            OnMoreGameClosed();
            return false;
        }

        //Interstitial Section
        public void setInterstitialAutoCache(bool _auto)
        {       
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }

        public void setInterstitialLocationList(string[] arrayOfLocations)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }

        //Interstitial Section
        public void setInterstitialAnimationType(APCPDefine.AnimationType _type)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name + ": " + _type);
        }

        public bool isInterstitialCached(string location)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name + ", location: " + location);
            OnMoreGameClosed();
            return false;
        }

        public void cacheAllInterstitials()
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }

        public void cacheInterstitial(string location)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name + ", location: " + location);
        }

        public bool showInterstitial(string location)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name + ", location: " + location);
            OnInterstitialClosed();
            return false;
        }

        public void removeDeepLinkData()
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name );
        }

        public string getDeepLinkData()
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name );
            return string.Empty;
        }

        public void setConsent(bool _consentGiven)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
        }

        public bool isConsentGiven()
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name);
            return false;
        }

        public void showConsentDialog(string _privacyPolicyUrl)
        {
            if(APCPSetting.isLogging)
                Debug.Log("Dummy " + MethodBase.GetCurrentMethod().Name + ", privacyPolicyUrl: " + _privacyPolicyUrl);
            OnConsentReturned(true);
        }
    }
    #endif
}
