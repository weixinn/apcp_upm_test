#import "APCPromo.h"
#import "APCPClient.h"
#import "CommonLib.h"


extern UIViewController* UnityGetGLViewController();

@interface APCPClient()<APCPDelegate>
@property(nonatomic) BOOL isLogOn;
@end


static APCPClient *instance = NULL;
static BOOL _isLogOn = false;

@implementation APCPClient


+(instancetype) getInstance
{
    if(instance == NULL)
        instance = [[APCPClient alloc] init];
    
    return instance;
}

- (void) initClient:(NSString *) gameId isDebugMode:(BOOL) isDebug
{
    _isLogOn = isDebug;
    
    [APCPromo initialize:gameId delegate:self testMode:isDebug];
}

- (void) connect
{
    [APCPromo connect];
}

- (void) setCustomId:(NSString *)customId
{
    [APCPromo setCustomId:customId];
}

- (void) setCustomLang:(NSString *)customLanguage
{
    [APCPromo setCustomLang:customLanguage];
}

- (void) setMoreGameCacheOnStartOnly:(BOOL)startOnly
{
    if(_isLogOn)
        NSLog(@"setMoreGameCacheOnStartOnly: %@", startOnly?@"YES": @"NO");
    
    [APCPromo setAutoCacheMoreGameOnStartOnly:startOnly];
}

- (void) setMoreGameAutoCache:(BOOL)isAutoCache
{
    [APCPromo setAutoCacheMoreGame:isAutoCache];
}

- (void) setMoreGameAnimationType:(int) aniType
{
    [APCPromo setMoreGameAnimationType:[self getAnimationTypeByInt:aniType]];
}

- (BOOL) isMoreGameReady
{
    return [APCPromo isMoreGameReady];
}

- (void) cacheMoreGame
{
    return [APCPromo cacheMoreGames];
}

- (BOOL) showMoreGame
{
    return [APCPromo showMoreGames:UnityGetGLViewController()];
}

- (void) setInterstitialAutoCache:(BOOL)isAutoCache
{
    [APCPromo setAutoCacheInterstitial:isAutoCache];
}

- (void) setInterstitialLocationList:(NSArray *)locationList
{
    [APCPromo setInterstitialLocationList:locationList];
}

- (void) setInterstitialAnimationType:(int) aniType
{
    [APCPromo setInterstitialAnimationType:[self getAnimationTypeByInt:aniType]];
}

- (BOOL) isInterstitialReady:(NSString *)location
{
    return [APCPromo isInterstitialReady:location];
}

- (void) cacheInterstitial:(NSString *)location
{
    [APCPromo cacheInterstitial:location];
}

- (void) cacheAllInterstitials
{
    [APCPromo cacheAllInterstitials];
}

- (BOOL) showInterstitial:(NSString *)location
{
    return [APCPromo showInterstitial:location RootView:UnityGetGLViewController()];
}

- (void) removeDeepLinkData
{
    [APCPromo removeDeepLinkData];
}

-(NSString *) getDeepLinkData
{
    return [APCPromo getDeepLinkData];
}

- (kAPCPViewAnimation) getAnimationTypeByInt:(int)type
{
    switch (type)
    {
        case 0:
        default:
            return kViewAnimationSlideFromBottom;
            
        case 1:
            return kViewAnimationSlideFromTop;
            
        case 2:
            return kViewAnimationSlideFromLeft;
            
        case 3:
            return kViewAnimationSlideFromRight;
        
    }
}

- (void) setConsent:(BOOL) consentGiven
{
    [APCPromo setConsent:consentGiven];
}

- (BOOL) isConsentGiven
{
    return [APCPromo isConsentGiven];
}

- (void) showConsentDialog: (NSString *) privacyPolicyUrl
{
    [APCPromo showConsentDialog:privacyPolicyUrl RootView:UnityGetGLViewController()];
}

#pragma mark APCPromoDelegate Methods
- (void)APCPMoreGamesCachedReady
{
    if(_isLogOn)
        NSLog(@"APCPClient::APCPMoreGamesCachedReady");
    
}

- (void)APCPMoreGamesCachedError:(NSString *)errorMsg
{
    if(_isLogOn)
        NSLog(@"APCPClient::APCPMoreGamesCachedError: %@",errorMsg);
}

- (void)APCPMoreGamesClosed
{
    if(_isLogOn)
        NSLog(@"APCPClient::APCPMoreGamesClosed");
    
    if(self.moreGameCloseCallback)
        self.moreGameCloseCallback();
}

- (void)APCPInitDone
{
    if(_isLogOn)
        NSLog(@"APCPClient::APCPInitDone");
    
    if(self.initDoneCallback)
        self.initDoneCallback();
}

- (void)APCPInitError:(NSString *) errorMsg
{
    if(_isLogOn)
        NSLog(@"APCPClient::APCPInitError: %@",errorMsg);
    
    if(self.initFailCallback)
        self.initFailCallback(MakeStringFromCopy(errorMsg));
}

- (void)APCPInterstitialClosed:(NSString *)location
{
    if(_isLogOn)
        NSLog(@"APCPClient::APCPInterstitialClosed");
    
    if(self.interstitialCloseCallback)
        self.interstitialCloseCallback(MakeStringFromCopy(location));
}

- (void)APCPInterstitialCachedReady:(NSString *)location
{
    if(_isLogOn)
        NSLog(@"APCPClient::APCPInterstitialCachedReady=> Location: %@",location);
}

- (void)APCPInterstitialCachedError:(NSString *)location ErrorMsg:(NSString *) errorMsg
{
    if(_isLogOn)
        NSLog(@"APCPClient::APCPInterstitialCachedError=> Location: %@, Error: %@",location, errorMsg);
}

- (void)APCPConsentDialogResult:(BOOL) consentGiven
{
    if(_isLogOn)
        NSLog(@"APCPClient::APCPConsentDialogResult=> consentGiven: %@",consentGiven?@"True":@"False");
    
    if(self.consentReturnCallback)
        self.consentReturnCallback(consentGiven);
}

@end
