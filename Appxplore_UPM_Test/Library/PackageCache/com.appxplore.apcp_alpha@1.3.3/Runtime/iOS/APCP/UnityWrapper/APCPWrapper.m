#import "CommonLib.h"
#import "APCPClient.h"

void APCPConstructWithCallbacks(APCPClientInitDoneCallback initDoneCallback,
                                APCPClientInitFailCallback initFailCallback,
                                APCPClientMoreGameClosedCallback moreGameClosedCallback,
                                APCPClientInterstitialClosedCallback interstitialClosedCallback,
                                APCPClientConsentReturnCallback consentReturnCallback)
{
    APCPClient *client = [APCPClient getInstance];
    client.initDoneCallback = initDoneCallback;
    client.initFailCallback = initFailCallback;
    client.moreGameCloseCallback = moreGameClosedCallback;
    client.interstitialCloseCallback = interstitialClosedCallback;
    client.consentReturnCallback = consentReturnCallback;
}

void APCPInitialize(const char* gameId, BOOL isDebugMode)
{
    [[APCPClient getInstance]initClient:CreateNSString(gameId) isDebugMode:isDebugMode];
}

void APCPConnect()
{
    [[APCPClient getInstance] connect];
}

void APCPSetCustomId(const char* customId)
{
    [[APCPClient getInstance] setCustomId:CreateNSString(customId)];
}

void APCPSetCustomLang(const char* customLang)
{
    [[APCPClient getInstance] setCustomLang:CreateNSString(customLang)];
}

void APCPSetMoreGameCacheOnStartOnly (BOOL cacheStart)
{
    [[APCPClient getInstance] setMoreGameCacheOnStartOnly:cacheStart];
}

void APCPSetMoreGameAutoCache(BOOL isAutoCache)
{
    [[APCPClient getInstance]setMoreGameAutoCache:isAutoCache];
}

void APCPSetMoreGameAnimationType(int _type)
{
    [[APCPClient getInstance]setMoreGameAnimationType:_type];
}

BOOL APCPCheckMoreGameReady()
{
    return [[APCPClient getInstance] isMoreGameReady];
}

void APCPCacheMoreGame()
{
    [[APCPClient getInstance] cacheMoreGame];
}

BOOL APCPShowMoreGame()
{
    return [[APCPClient getInstance] showMoreGame];
}

void APCPSetInterstitialAutoCache(BOOL isAutoCache)
{
    [[APCPClient getInstance]setInterstitialAutoCache:isAutoCache];
}

void APCPSetInterstitialsLocationList(const char* locations[], int length)
{
    [[APCPClient getInstance]setInterstitialLocationList:CreateNSArray(locations, length)];
}

void APCPSetInterstitialAnimationType(int _type)
{
    [[APCPClient getInstance]setInterstitialAnimationType:_type];
}

BOOL APCPCheckInterstitialReady(const char * location)
{
    return [[APCPClient getInstance] isInterstitialReady:CreateNSString(location)];
}

void APCPCacheInterstitial(const char * location)
{
    [[APCPClient getInstance] cacheInterstitial:CreateNSString(location)];
}

void APCPCacheAllInterstitials()
{
    [[APCPClient getInstance] cacheAllInterstitials];
}

BOOL APCPShowInterstitial(const char * location)
{
    return [[APCPClient getInstance] showInterstitial:CreateNSString(location)];
}

void APCPRemoveDeepLinkData()
{
    [[APCPClient getInstance]removeDeepLinkData];
}

const char * APCPGetDeepLinkData()
{
    NSString * data = [[APCPClient getInstance] getDeepLinkData];
    return MakeStringFromCopy(data);
}

void APCPSetConsent(bool consentGiven)
{
    [[APCPClient getInstance]setConsent:consentGiven];
}

BOOL APCPIsConsentGiven()
{
    return [[APCPClient getInstance] isConsentGiven];
}

void APCPShowConsentDialog(const char * privacyPolicyUrl)
{
    [[APCPClient getInstance] showConsentDialog: CreateNSString(privacyPolicyUrl)];
}

