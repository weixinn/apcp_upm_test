#import "APCPromo.h"

typedef void (*APCPClientInitDoneCallback)();
typedef void (*APCPClientInitFailCallback)(const char *error);
typedef void (*APCPClientMoreGameClosedCallback)();
typedef void (*APCPClientInterstitialClosedCallback)(const char* location);
typedef void (*APCPClientConsentReturnCallback)(bool consentGiven);

@interface APCPClient : NSObject

@property(nonatomic, assign) APCPClientInitDoneCallback initDoneCallback;
@property(nonatomic, assign) APCPClientInitFailCallback initFailCallback;
@property(nonatomic, assign) APCPClientMoreGameClosedCallback moreGameCloseCallback;
@property(nonatomic, assign) APCPClientInterstitialClosedCallback interstitialCloseCallback;
@property(nonatomic, assign) APCPClientConsentReturnCallback consentReturnCallback;

+(instancetype) getInstance;

- (void) initClient:(NSString *)gameId isDebugMode:(BOOL) isDebug;
- (void) connect;

- (void) setCustomId:(NSString *)customId;
- (void) setCustomLang:(NSString *)customLanguage;

- (void) setMoreGameCacheOnStartOnly:(BOOL)startOnly;
- (void) setMoreGameAutoCache:(BOOL)isAutoCache;
- (void) setMoreGameAnimationType:(int) aniType;
- (BOOL) isMoreGameReady;
- (void) cacheMoreGame;
- (BOOL) showMoreGame;

- (void) setInterstitialAutoCache:(BOOL)isAutoCache;
- (void) setInterstitialLocationList:(NSArray *)locationList;
- (void) setInterstitialAnimationType:(int) aniType;
- (BOOL) isInterstitialReady:(NSString *)location;
- (void) cacheInterstitial:(NSString *)location;
- (void) cacheAllInterstitials;
- (BOOL) showInterstitial:(NSString *)location;

- (void) removeDeepLinkData;
- (NSString *) getDeepLinkData;

- (void) setConsent:(BOOL) consentGiven;
- (BOOL) isConsentGiven;
- (void) showConsentDialog: (NSString *) privacyPolicyUrl;
@end
