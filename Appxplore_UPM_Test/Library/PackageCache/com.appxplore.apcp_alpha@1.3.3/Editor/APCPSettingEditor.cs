﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEditor;
using Appxplore.APCP;
using Appxplore.EditorUtils;

[CustomEditor(typeof(APCPSetting))]
public class APCPSettingEditor : Editor 
{
    GUIStyle TitleStyle;
    GUIStyle subTitleStyle;

    GUIContent APCPTitle = new GUIContent("AppxPlore CrossPromotion Setting");
    GUIContent APCPGeneral = new GUIContent("General");
    GUIContent APCPMoreGame = new GUIContent("More Game");
    GUIContent APCPInterstitial = new GUIContent("Interstitials");
    //GUIContent APCPVersionStr = new GUIContent("Version");


    GUIContent enableLoggingToggle = new GUIContent("Debug Mode");
    GUIContent enableAutoCache = new GUIContent("AutoCache");
    GUIContent cacheOnStart = new GUIContent("CacheOnStart");
    GUIContent gameIdLabel = new GUIContent("Game Id");
    GUIContent urlSchemeLabel = new GUIContent("Url Scheme");
    GUIContent defaultAniLabel = new GUIContent("Default Animation Type");
  

    private APCPSetting instance;
    private string mHelpBox = "Click on ? to know the function of the Section\n\n";
    private string mGameIdHelpStr = "Game Id of this game\n\n";
    private string mUrlSchemeHelpStr = "Use this as the start url scheme for DeepLink\n\n";
    private string mMoreGameAniStr = "Default More Game Animation Type\n\n";
    private string mInterstitialAniStr = "Default Interstitial Animation Type\n\n";

    private const string RULES_TEMPLATE_PATH = "Assets/AppxplorePlugin/APCP/Editor/Android/APCP.manifestrules.template";
    private const string RULES_PATH = "Assets/AppxplorePlugin/APCP/Editor/Android/APCP.manifestrules.json";
    private const string REPLACE_WORD = "[new_value_here]";


    public override void OnInspectorGUI() 
    {
        serializedObject.Update();

        if (instance == null) 
        {
            instance = Resources.Load("apcpSettings") as APCPSetting;
            TitleStyle = new GUIStyle();
            TitleStyle.fontStyle = FontStyle.Bold;
            TitleStyle.fontSize = 14;
            TitleStyle.normal.textColor = Color.white;
            TitleStyle.alignment = TextAnchor.MiddleCenter;

            subTitleStyle = new GUIStyle();
            subTitleStyle.fontStyle = FontStyle.Bold;
            subTitleStyle.fontSize = 11;
            subTitleStyle.normal.textColor = Color.white;
            subTitleStyle.alignment = TextAnchor.MiddleLeft;

        }

        SetupUI ();
    }

    private void SetupUI() 
    {
        EditorGUILayout.LabelField(APCPTitle, TitleStyle);
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        //if(!string.IsNullOrEmpty(mHelpBox))
        EditorGUILayout.BeginVertical();
        EditorGUILayout.HelpBox(mHelpBox, MessageType.None);

        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        //Version
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(string.Format("Version: {0}", APCPSetting.VERSION),subTitleStyle);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        EditorGUILayout.LabelField(APCPGeneral, subTitleStyle);
        EditorGUI.indentLevel++;

        // Logging toggle.
        EditorGUILayout.BeginHorizontal();
        instance.enableLogging(EditorGUILayout.Toggle(enableLoggingToggle, APCPSetting.isLogging));
        EditorGUILayout.EndHorizontal();

        //GameId
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(gameIdLabel);
        if (GUILayout.Button ("?", EditorStyles.miniButton, GUILayout.Width(25f)))
            mHelpBox = mGameIdHelpStr;
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        instance.setGameId(EditorGUILayout.TextField(APCPSetting.GameId));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        //Deep Link Url
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(urlSchemeLabel);
        if (GUILayout.Button ("?", EditorStyles.miniButton, GUILayout.Width(25f)))
            mHelpBox = mUrlSchemeHelpStr;
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        instance.setURLScheme(EditorGUILayout.TextField(APCPSetting.UrlScheme));
        EditorGUILayout.EndHorizontal();


        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Generate/Update AndroidManifest(android)", EditorStyles.miniButtonMid))
            generateAndroidManifest();
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();


        EditorGUILayout.EndVertical();
        EditorGUI.indentLevel--;

        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        EditorGUILayout.LabelField(APCPMoreGame, subTitleStyle);
        EditorGUI.indentLevel++;
        //More Game AnimationType
        instance.enableAutoCacheMoreGame(EditorGUILayout.Toggle(enableAutoCache, APCPSetting.isAutoCachedMoreGame));
        instance.enableAutoCacheMoreGameOnStart(EditorGUILayout.Toggle(cacheOnStart, APCPSetting.isAutoCachedMoreGameOnStart));
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(defaultAniLabel);
        instance.setMoreGameAnimationType((APCPDefine.AnimationType)(EditorGUILayout.EnumPopup(APCPSetting.MoreGameAnimationType)));
        if (GUILayout.Button ("?", EditorStyles.miniButton, GUILayout.Width(25f)))
            mHelpBox = mMoreGameAniStr;
        EditorGUILayout.EndHorizontal();
        EditorGUI.indentLevel--;

        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        //Interstitial AnimationType
        EditorGUILayout.LabelField(APCPInterstitial, subTitleStyle);
        EditorGUI.indentLevel++;
        instance.enableAutoCacheInterstitial(EditorGUILayout.Toggle(enableAutoCache, APCPSetting.isAutoCachedInterstitial));
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(defaultAniLabel);
        instance.setInterstitialAnimationType((APCPDefine.AnimationType)(EditorGUILayout.EnumPopup(APCPSetting.InterstitialAnimationType)));
        if (GUILayout.Button ("?", EditorStyles.miniButton, GUILayout.Width(25f)))
            mHelpBox = mInterstitialAniStr;
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Interstitials"), true);
        serializedObject.ApplyModifiedProperties();
        EditorGUI.indentLevel--;

        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    }

    void generateAndroidManifest()
    {
        if (APCPSetting.UrlScheme.Length <= 0)
            EditorUtility.DisplayDialog("URL Scheme Empty", "Url Scheme is Empty!", "OK");
        else 
            updateAPCPRulesManifest();
    }

    void updateAPCPRulesManifest()
    {

        if(!File.Exists(RULES_PATH))
        {
            string content = File.ReadAllText(RULES_TEMPLATE_PATH);
            content = content.Replace(REPLACE_WORD, APCPSetting.UrlScheme);
            File.WriteAllText(RULES_PATH, content);
        }


        if (ManifestMod.LoadManifest())
        {
            Debug.Log("Loaded Manifest and Update Rule File soon.");
            ManifestMod.updateOneRuleFiles(RULES_PATH);
        }

        if(EditorUtility.DisplayDialog("Generate/Update AndroidManifest.xml done!", "Please check on the console panel for any error message.", "OK"))
            AssetDatabase.Refresh();
    }
}

