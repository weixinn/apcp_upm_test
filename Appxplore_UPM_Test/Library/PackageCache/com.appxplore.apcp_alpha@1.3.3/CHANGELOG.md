version                 : 1.3.2
====================================================================================
release date            : 11th March 2020
android version         : 1.3.3
ios version             : 1.3.0
unity version           : 2017f4
common plugin           : 1.2.0
play service resolver   : v1.2.121.0
android-gif-drawable    : 1.2.19

** Change Logs: **
1. [Unity Android] Added dependencies.xml so we need to include the install referrer library at the client side.


version                 : 1.3.1
====================================================================================
release date            : 03rd Jan 2020
android version         : 1.3.2
ios version             : 1.3.0
unity version           : 2017f4
common plugin           : 1.2.0
play service resolver   : v1.2.121.0
android-gif-drawable    : 1.2.19
**********************************************************************************************************
** Breaking Changes, please do the following before proceed if you are old user for upgrade:
**
** Delete following files/folder:
** - AppxplorePlugin/Common/Scripts
** - AppxplorePlugin/APCP/Editor/Android/APCP.manifestrules.json
**
** Remove the following from /Plugins/Android/AndroidManifest.xml:
**   <!-- [Start] Used for Campaign Measurement -->
**   <receiver android:name="com.appxplore.apcp.Analytics.CampaignTrackingReceiver" android:exported="true" >
**     <intent-filter>
**       <action android:name="com.android.vending.INSTALL_REFERRER" />
**     </intent-filter>
**   </receiver>
**   <!-- [End] Used for Campaign Measurement -->
** This is due to Google deprecate the Broadcastreceiver install referrer intent.
**********************************************************************************************************

** Change Logs: **
1. [Android] Fix the BroadcastReceiver Intent is deprecated starting from March 2020, according to the link below:
https://android-developers.googleblog.com/2019/11/still-using-installbroadcast-switch-to.html


version                 : 1.3.0
====================================================================================
release date            : 26th November 2019
android version         : 1.3.1
ios version             : 1.3.0
unity version           : 2017f4
common plugin           : 1.1.1
play service resolver   : v1.2.121.0
android-gif-drawable    : 1.2.19
*************************************************************************************************
** Change Logs: **
1. [Android] Upgrade android-gif-drawable due to a vulnerability stated here: https://nvd.nist.gov/vuln/detail/CVE-2019-11932 
2. Drop the support for Unity3d 5.6, now we support at least Unity3d 2017 LTS version.


version                 : 1.2.1
====================================================================================
release date            : 05th July 2019
android version         : 1.3.1
ios version             : 1.3.0
unity version           : 5.6.7f2
common plugin           : 1.1.1
play service resolver   : v1.2.121.0
android-gif-drawable    : 1.2.8
*************************************************************************************************
** Change Logs: **
1. [Android] Fix Crash during Interstitial On Play.
2. Upgrade PlayServiceResolver too v1.2.121.0
3. Upgrade Common Plugin to 1.1.1


version                 : 1.2.0
====================================================================================
release date            : 25th Feb 2019
android version         : 1.3.0
ios version             : 1.3.0
unity version           : 5.6.6f2
common plugin           : 1.1.0
play service resolver   : v1.2.99.0
android-gif-drawable    : 1.2.8

*************************************************************************************************
** Breaking Changes, please do the following before proceed if you are old user for upgrade:
** Delete following files/folder:
** - AppxplorePlugin/Common/Editor/ManifestMod
** - PlayServiceResolver
*************************************************************************************************

** Change Logs: **
1. Upgrade to tally the common plugin and play service resolver.


version                    : 1.1.0
====================================================================================
release date            : 16th August 2018
android version            : 1.3.0
ios version                : 1.3.0
unity version            : 5.6.6f2
common plugin            : 1.0.0
play service resolver    : v1.2.72.0
android-gif-drawable    : 1.2.8
*************************************************************************************************
** Change Logs: **
1. Added the Gif Interstitials in Native SDK.
2. [Android] Added android-gif-drawable aar to support Gif Interstitial.
3. Raise the iOS Support Version to iOS 8.0 and above.


version                    : 1.0.0
====================================================================================
release date            : 20th July 2018
android version            : 1.2.2
ios version                : 1.2.0
unity version            : 5.6.6f2
common plugin            : 1.0.0
play service resolver    : v1.2.72.0
*************************************************************************************************
** Upgrade Method: **
1. In order to upgrade to this version, please remove these files/folders:
    - AppxplorePlugin/Common
    - Editor/XUPorter
    - PlayServicesResolver

** Change Logs: **
1. change the format of the version to x.x.x
2. update to use the common plugin method to handle all the common methods.
3. provide the method to straight away update the AndroidManifest.xml instead of using old way.
4. fix the generate new resources folder error when you generate the new APCPSettings.asset file.


Unity Plugin version: 2018062200
====================================================================================
Android Version: 1.2.2
iOS Version: 1.2.0
1. [Android, Unity] Fix the Consent Dialog crash over not supported languages due to no default languages is specified.


Unity Plugin version: 2018061900
====================================================================================
Android Version: 1.2.1
iOS Version: 1.2.0
1. [Android, Unity] Fix the Dialog Button cover each other when translation is too long during consent dialog.

Unity Plugin version: 2018061300
====================================================================================
iOS Version: 1.2.0
Android Version: 1.2.0
1. [iOS, Android, Unity] Create the GPDR Consent Dialog to be used in the game, please refer to Documentation for more details.
2. [iOS, Android, Unity] [***Very Important***] Due to Consent Dialog, we split out the connect function from initialization function, please look at this carefully when integrate the SDK.
3. [Android] Fix the CampaignTrackingReceiver crash log issue according to Android Vital

version: 2018052400
====================================================================================
1. [iOS] Fix the Archive issue in iOS Build.
2. [Unity] Change the Setting Version to 2018052400, instead of remain 20171129_0

version: 20180514_0
====================================================================================
1. [Android] Version: 1.1.10
2. [Android] Fix the CampaignTrackingReceiver crash log issue according to Android Vital(not really happens on our build).
3. [Android] Add the Device ID to be shown when it is in debug mode, you need to find it from Android Monitor log with "ACPromo" Tag and Info. 
4. [Android] Please remove the CampaignTrackingService from your Android Manifest.
      <service android:name="com.appxplore.apcp.Analytics.CampaignTrackingService" />
5. [iOS] Version: 1.1.10
6. [iOS] Fix the crash log in InterstitialData from iOS Organizer.
7. [iOS] Add the Device ID to be shown when it is in debug mode, you need to find it from the logs with the name "Device ID".
8. [Android] Try fix the Interstitials auto-close before finish loading animation by keep clicking on the screen.
9. [iOS] Prevent multiple click event sent to server straight away.
10. [iOS] Add more space for More Games in iPhoneX.

version: 20171129_0
====================================================================================
1. [iOS, Android, Unity] Upgrade the SDK so it will suit the new * Location Settings. All location using same cached data except those specified during initialization.
2. [Android] Fix Interstitial Screen mis-layout.
3. [Android] Fix More Game Screen mis-layout on Landscape Mode.

version: 20171102_0
====================================================================================
2. [Android] Fix some exception from Android Vital(according to Trello)

version: 20171002_0
====================================================================================
1. [Unity] Added Gradle and Proguard Support for Unity 5.6.3f1 onwards.
2. [Android] Fix some classes missing in Android aar files.

version: 20170926_0
====================================================================================
1. [iOS] Support the layout of iPhone X for More Games and Interstitials.
2. [Android] Try fix the crash on Android for MoreGameActivity due to null bitmap Image.
3. [Android] Try fix the crash on Android for InterstitialActivity due to null bitmap Image.

version: 20170816_0
====================================================================================
1. [iOS] Try fix crash on iOS from Organizer Log about the findImageOperation function in APCPImageManager
2. [Android] Try fix the crash on Android due to possible of TargetName is null on ImageThread.
3. [Android] Try fix the crash on Android in MoreGameGridViewAdapter as some error has occurred.


version: 20170713_0
====================================================================================
1. [Android] Make sure the Adverstising Id from Device will not be empty or null.

version: 20170712_0
====================================================================================
1. [Android] Try Fix crash on Android due to ImageId is null on the ImageThread
2. [iOS] Try Fix the crash on iOS from Organizer Log
3. [UnityPlugin] Fix the APCPPostProcess Script that override the Facebook URLScheme Setting.
4. [UnityPlugin] Add the UNITY_STANDALONE as dummy client as per request.

version: 20170620_0
====================================================================================
** If you are facing URL Scheme Issue for FacebookSDK. You may want to try to put the FacebookPostProcess.cs in this folder in your Editor Folder in the game. That should help solve the issue. currently FB SDK 7.8.0 required “fb{fbid}fb{fbid}” format to be included in the URL Scheme. if yours are different, please modify the FacebookPostProcess.cs accordingly.

1. [Android] Fix crash on Android due to getImageFileName might be null on InterstitialData
2. [Android] Fix click on own deeplink interstitial has animation before close. Should be directly close.

version: 20170525_0
====================================================================================
1. [iOS] Fix Deeplink are not auto start the iOS App.
2. [iOS] Fix using Vendor ID if Ads ID is not available.
3. [iOS, Android] Fix Alternative URL for Interstitial and More Game.

version: 20170523_0
====================================================================================
1. [UnityPlugin] A button to remove printing Debug.Log to Unity Editor
2. [iOS] Try fix crashes shown in XCode Organizer(MoreGameManager & InterstitialView).
3. [iOS] Fix scrolling down in More Game Page will have some cached image due to reuse old collectionCell.
4. [Android] Fix Interstitial Layout incorrect in certain devices.
5. [Android] Fix MoreGame Page deeplink to another game and return to current game(by using home page) will make more game page closed without notify the callback.

version: 20170426_0
====================================================================================
1. [UnityPlugin] Fix the problem of deep linking open url function being replaced by APCPAppController class. Now it should forward back to parent class before do anything.
2. [UnityPlugin] add URL Scheme Name for URL Deep Linking.
3. [iOS] remove the log for the analytics tracking to avoid ppl from breaking into our server(debug mode still visible, rmb to turn it off when production mode).

Changelog 20170418_0
====================================================================================
1. [iOS, Android, UnityPlugin] Settle the Session Token invalid issue.
2. [iOS, Android] upgrade both version to 1.1.0 version code 10100
3. [iOS, Android, UnityPlugin] Deep Link feature is now Available, set it from AppxplorePlugin-> Edit APCP Settings-> Url Scheme
4. [Android, UnityPlugin] Campaign Tracking Client Side is ready.

Changelog 20170411_0
====================================================================================
1. [Android] now close button in Interstitial should have bigger hit area.
2. [Android] Interstitial Mask now won't slide out upon interstitial closed.
3. [iOS, Android, UnityPlugin] now click of Interstitial will be only for the content Image and also the "Button" in the Frame.

Changelog 20170407_0
====================================================================================
1. Add the Location Key for Interstitial View and Click for Reporting API
2. [iOS] Fix APCPResources Bundle can’t submit to App Store Problem.

Changelog 20170403_0
====================================================================================
1. Add in the More Game View Event to Analytics
2. Fix BitCode Enable Problem.

Changelog 20170328_0
====================================================================================
1. Fix the Final Presentation way of More Games.

Changelog 20170324_0
====================================================================================
1. Fix Bug for Multiple Clicks and Show More Game.
2. Change the Presentation Way of Once Interstitial Clicked, it will directly close the interstitial without animation and go to the store.


Changelog 20170323_0
====================================================================================
1. Change the Layout of APCP SDK. Should be Final for Interstitials. Hopefully done for More Game Too.
2. Added a feature for Once Cached More Games won’t be deleted. Tweak at APCP Settings.
3. Fix Bug for Multiple Clicks and Show More Game.
