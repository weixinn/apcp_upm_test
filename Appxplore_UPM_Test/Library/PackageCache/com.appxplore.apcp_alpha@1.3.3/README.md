# Appxplore APCP Plugin

Plugin for Appxplore internal use.

# Release Notes

## version                 : 1.3.2

Release date            : 11th March 2020<br/>
Android version         : 1.3.3<br/>
iOS version             : 1.3.0<br/>
Unity version           : 2017f4<br/>
Common plugin           : 1.2.0<br/>
Play service resolver   : v1.2.121.0<br/>
Android-gif-drawable    : 1.2.19

**Change Logs:**<br/>
1. [Unity Android] Added dependencies.xml so we need to include the install referrer library at the client side.

<br/>
<br/>

## version                 : 1.3.1

Release date            : 03rd Jan 2020<br/>
Android version         : 1.3.2<br/>
iOS version             : 1.3.0<br/>
Unity version           : 2017f4<br/>
Common plugin           : 1.2.0<br/>
Play service resolver   : v1.2.121.0<br/>
Android-gif-drawable    : 1.2.19
**********************************************************************************************************
** Breaking Changes, please do the following before proceed if you are old user for upgrade:<br/>
**<br/>
** Delete following files/folder:<br/>
** - AppxplorePlugin/Common/Scripts<br/>
** - AppxplorePlugin/APCP/Editor/Android/APCP.manifestrules.json<br/>
**<br/>
** Remove the following from /Plugins/Android/AndroidManifest.xml:<br/>
**   <!-- [Start] Used for Campaign Measurement --><br/>
**   <receiver android:name="com.appxplore.apcp.Analytics.CampaignTrackingReceiver" android:exported="true" ><br/>
**     <intent-filter><br/>
**       <action android:name="com.android.vending.INSTALL_REFERRER" /><br/>
**     </intent-filter><br/>
**   </receiver><br/>
**   <!-- [End] Used for Campaign Measurement --><br/>
** This is due to Google deprecate the Broadcastreceiver install referrer intent.
**********************************************************************************************************

**Change Logs:**<br/>
1. [Android] Fix the BroadcastReceiver Intent is deprecated starting from March 2020, according to the link below:<br/>
https://android-developers.googleblog.com/2019/11/still-using-installbroadcast-switch-to.html

<br/>
<br/>

## version                 : 1.3.0

Release date            : 26th November 2019<br/>
Android version         : 1.3.1<br/>
iOS version             : 1.3.0<br/>
Unity version           : 2017f4<br/>
Common plugin           : 1.1.1<br/>
Play service resolver   : v1.2.121.0<br/>
Android-gif-drawable    : 1.2.19
*************************************************************************************************
**Change Logs:**<br/>
1. [Android] Upgrade android-gif-drawable due to a vulnerability stated here: https://nvd.nist.gov/vuln/detail/CVE-2019-11932 <br/>
2. Drop the support for Unity3d 5.6, now we support at least Unity3d 2017 LTS version.

<br/>
<br/>

## version                 : 1.2.1

Release date            : 05th July 2019<br/>
Android version         : 1.3.1<br/>
iOS version             : 1.3.0<br/>
Unity version           : 5.6.7f2<br/>
Common plugin           : 1.1.1<br/>
Play service resolver   : v1.2.121.0<br/>
Android-gif-drawable    : 1.2.8
*************************************************************************************************
**Change Logs:**<br/>
1. [Android] Fix Crash during Interstitial On Play.<br/>
2. Upgrade PlayServiceResolver too v1.2.121.0<br/>
3. Upgrade Common Plugin to 1.1.1

<br/>
<br/>

## version                 : 1.2.0

Release date            : 25th Feb 2019<br/>
Android version         : 1.3.0<br/>
iOS version             : 1.3.0<br/>
Unity version           : 5.6.6f2<br/>
Common plugin           : 1.1.0<br/>
Play service resolver   : v1.2.99.0<br/>
Android-gif-drawable    : 1.2.8

*************************************************************************************************
** Breaking Changes, please do the following before proceed if you are old user for upgrade:<br/>
** Delete following files/folder:<br/>
** - AppxplorePlugin/Common/Editor/ManifestMod<br/>
** - PlayServiceResolver<br/>
*************************************************************************************************

**Change Logs:**<br/>
1. Upgrade to tally the common plugin and play service resolver.

<br/>
<br/>

## version                    : 1.1.0

Release date            : 16th August 2018<br/>
Android version            : 1.3.0<br/>
iOS version                : 1.3.0<br/>
Unity version            : 5.6.6f2<br/>
Common plugin            : 1.0.0<br/>
Play service resolver    : v1.2.72.0<br/>
Android-gif-drawable    : 1.2.8
*************************************************************************************************
**Change Logs:**<br/>
1. Added the Gif Interstitials in Native SDK.<br/>
2. [Android] Added android-gif-drawable aar to support Gif Interstitial.<br/>
3. Raise the iOS Support Version to iOS 8.0 and above.

<br/>
<br/>

## version                    : 1.0.0

Release date            : 20th July 2018<br/>
Android version            : 1.2.2<br/>
iOS version                : 1.2.0<br/>
Unity version            : 5.6.6f2<br/>
Common plugin            : 1.0.0<br/>
Play service resolver    : v1.2.72.0
*************************************************************************************************
**Upgrade Method:**<br/>
1. In order to upgrade to this version, please remove these files/folders:<br/>
    - AppxplorePlugin/Common<br/>
    - Editor/XUPorter<br/>
    - PlayServicesResolver<br/>

**Change Logs:**<br/>
1. Change the format of the version to x.x.x<br/>
2. Update to use the common plugin method to handle all the common methods.<br/>
3. Provide the method to straight away update the AndroidManifest.xml instead of using old way.<br/>
4. Fix the generate new resources folder error when you generate the new APCPSettings.asset file.

<br/>
<br/>

Unity Plugin version: 2018062200
====================================================================================
Android Version: 1.2.2<br/>
iOS Version: 1.2.0<br/>
1. [Android, Unity] Fix the Consent Dialog crash over not supported languages due to no default languages is specified.

<br/>
<br/>


Unity Plugin version: 2018061900
====================================================================================
Android Version: 1.2.1<br/>
iOS Version: 1.2.0<br/>
1. [Android, Unity] Fix the Dialog Button cover each other when translation is too long during consent dialog.

<br/>
<br/>

Unity Plugin version: 2018061300
====================================================================================
iOS Version: 1.2.0<br/>
Android Version: 1.2.0<br/>
1. [iOS, Android, Unity] Create the GPDR Consent Dialog to be used in the game, please refer to Documentation for more details.<br/>
2. [iOS, Android, Unity] [***Very Important***] Due to Consent Dialog, we split out the connect function from initialization function, please look at this carefully when integrate the SDK.<br/>
3. [Android] Fix the CampaignTrackingReceiver crash log issue according to Android Vital

<br/>
<br/>

version: 2018052400
====================================================================================
1. [iOS] Fix the Archive issue in iOS Build.<br/>
2. [Unity] Change the Setting Version to 2018052400, instead of remain 20171129_0

<br/>
<br/>

version: 20180514_0
====================================================================================
1. [Android] Version: 1.1.10<br/>
2. [Android] Fix the CampaignTrackingReceiver crash log issue according to Android Vital(not really happens on our build).<br/>
3. [Android] Add the Device ID to be shown when it is in debug mode, you need to find it from Android Monitor log with "ACPromo" Tag and Info. <br/>
4. [Android] Please remove the CampaignTrackingService from your Android Manifest.
      <service android:name="com.appxplore.apcp.Analytics.CampaignTrackingService" /><br/>
5. [iOS] Version: 1.1.10<br/>
6. [iOS] Fix the crash log in InterstitialData from iOS Organizer.<br/>
7. [iOS] Add the Device ID to be shown when it is in debug mode, you need to find it from the logs with the name "Device ID".<br/>
8. [Android] Try fix the Interstitials auto-close before finish loading animation by keep clicking on the screen.<br/>
9. [iOS] Prevent multiple click event sent to server straight away.<br/>
10. [iOS] Add more space for More Games in iPhoneX.

<br/>
<br/>

version: 20171129_0
====================================================================================
1. [iOS, Android, Unity] Upgrade the SDK so it will suit the new * Location Settings. All location using same cached data except those specified during initialization.<br/>
2. [Android] Fix Interstitial Screen mis-layout.<br/>
3. [Android] Fix More Game Screen mis-layout on Landscape Mode.

<br/>
<br/>

version: 20171102_0
====================================================================================
2. [Android] Fix some exception from Android Vital(according to Trello)

<br/>
<br/>

version: 20171002_0
====================================================================================
1. [Unity] Added Gradle and Proguard Support for Unity 5.6.3f1 onwards.<br/>
2. [Android] Fix some classes missing in Android aar files.

<br/>
<br/>

version: 20170926_0
====================================================================================
1. [iOS] Support the layout of iPhone X for More Games and Interstitials.<br/>
2. [Android] Try fix the crash on Android for MoreGameActivity due to null bitmap Image.<br/>
3. [Android] Try fix the crash on Android for InterstitialActivity due to null bitmap Image.

<br/>
<br/>

version: 20170816_0
====================================================================================
1. [iOS] Try fix crash on iOS from Organizer Log about the findImageOperation function in APCPImageManager<br/>
2. [Android] Try fix the crash on Android due to possible of TargetName is null on ImageThread.<br/>
3. [Android] Try fix the crash on Android in MoreGameGridViewAdapter as some error has occurred.

<br/>
<br/>

version: 20170713_0
====================================================================================
1. [Android] Make sure the Adverstising Id from Device will not be empty or null.

<br/>
<br/>

version: 20170712_0
====================================================================================
1. [Android] Try Fix crash on Android due to ImageId is null on the ImageThread<br/>
2. [iOS] Try Fix the crash on iOS from Organizer Log<br/>
3. [UnityPlugin] Fix the APCPPostProcess Script that override the Facebook URLScheme Setting.<br/>
4. [UnityPlugin] Add the UNITY_STANDALONE as dummy client as per request.

<br/>
<br/>

version: 20170620_0
====================================================================================
** If you are facing URL Scheme Issue for FacebookSDK. You may want to try to put the FacebookPostProcess.cs in this folder in your Editor Folder in the game. That should help solve the issue. currently FB SDK 7.8.0 required “fb{fbid}fb{fbid}” format to be included in the URL Scheme. if yours are different, please modify the FacebookPostProcess.cs accordingly.<br/>

1. [Android] Fix crash on Android due to getImageFileName might be null on InterstitialData<br/>
2. [Android] Fix click on own deeplink interstitial has animation before close. Should be directly close.

<br/>
<br/>

version: 20170525_0
====================================================================================
1. [iOS] Fix Deeplink are not auto start the iOS App.<br/>
2. [iOS] Fix using Vendor ID if Ads ID is not available.<br/>
3. [iOS, Android] Fix Alternative URL for Interstitial and More Game.

<br/>
<br/>

version: 20170523_0
====================================================================================
1. [UnityPlugin] A button to remove printing Debug.Log to Unity Editor<br/>
2. [iOS] Try fix crashes shown in XCode Organizer(MoreGameManager & InterstitialView).<br/>
3. [iOS] Fix scrolling down in More Game Page will have some cached image due to reuse old collectionCell.<br/>
4. [Android] Fix Interstitial Layout incorrect in certain devices.<br/>
5. [Android] Fix MoreGame Page deeplink to another game and return to current game(by using home page) will make more game page closed without notify the callback.

<br/>
<br/>

version: 20170426_0
====================================================================================
1. [UnityPlugin] Fix the problem of deep linking open url function being replaced by APCPAppController class. Now it should forward back to parent class before do anything.<br/>
2. [UnityPlugin] add URL Scheme Name for URL Deep Linking.<br/>
3. [iOS] remove the log for the analytics tracking to avoid ppl from breaking into our server(debug mode still visible, rmb to turn it off when production mode).

<br/>
<br/>

Changelog 20170418_0
====================================================================================
1. [iOS, Android, UnityPlugin] Settle the Session Token invalid issue.<br/>
2. [iOS, Android] upgrade both version to 1.1.0 version code 10100<br/>
3. [iOS, Android, UnityPlugin] Deep Link feature is now Available, set it from AppxplorePlugin-> Edit APCP Settings-> Url Scheme<br/>
4. [Android, UnityPlugin] Campaign Tracking Client Side is ready.

<br/>
<br/>

Changelog 20170411_0
====================================================================================
1. [Android] now close button in Interstitial should have bigger hit area.<br/>
2. [Android] Interstitial Mask now won't slide out upon interstitial closed.<br/>
3. [iOS, Android, UnityPlugin] now click of Interstitial will be only for the content Image and also the "Button" in the Frame.

<br/>
<br/>

Changelog 20170407_0
====================================================================================
1. Add the Location Key for Interstitial View and Click for Reporting API<br/>
2. [iOS] Fix APCPResources Bundle can’t submit to App Store Problem.

<br/>
<br/>

Changelog 20170403_0
====================================================================================
1. Add in the More Game View Event to Analytics<br/>
2. Fix BitCode Enable Problem.

<br/>
<br/>

Changelog 20170328_0
====================================================================================
1. Fix the Final Presentation way of More Games.

<br/>
<br/>

Changelog 20170324_0
====================================================================================
1. Fix Bug for Multiple Clicks and Show More Game.<br/>
2. Change the Presentation Way of Once Interstitial Clicked, it will directly close the interstitial without animation and go to the store.

<br/>
<br/>

Changelog 20170323_0
====================================================================================
1. Change the Layout of APCP SDK. Should be Final for Interstitials. Hopefully done for More Game Too.<br/>
2. Added a feature for Once Cached More Games won’t be deleted. Tweak at APCP Settings.<br/>
3. Fix Bug for Multiple Clicks and Show More Game.
