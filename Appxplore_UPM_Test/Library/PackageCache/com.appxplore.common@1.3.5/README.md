# Appxplore Common Plugin

Plugin for Appxplore internal use.

# Release Notes

## version 1.3.1

Date:                       6th Jan 2021<br/>
Unity3d Version:            Unity3d 2017.4, Unity3d 2018.4<br/>
ExternalDependencyManager:  v1.2.163.0<br/>
JsonDotNet:                 v2.0.1

**********************************************************************************************************
** Breaking Changes, please do the following before proceed if you are old user for upgrade:<br/>
** Delete following files/folder:<br/>
** - PlayServiceResolver<br/>
** - AppxplorePlugin/Common/Editor/XUPorter
**********************************************************************************************************

**ChangeLogs:**<br/>
1. Updated ExternalDependencyManager to v1.2.163.0.

<br/>
<br/>
<br/>

## version 1.3.0

Date:                       21th September 2020<br/>
Unity3d Version:            Unity3d 2017.4, Unity3d 2018.4<br/>
ExternalDependencyManager:  v1.2.159.0<br/>
JsonDotNet:                 v2.0.1

**********************************************************************************************************
** Breaking Changes, please do the following before proceed if you are old user for upgrade:<br/>
** Delete following files/folder:<br/>
** - PlayServicesResolver<br/>
** - AppxplorePlugin/Common/Editor/XUPorter
**********************************************************************************************************

**ChangeLogs:**<br/>
1. Remove XUPorter, so you no need to include any .projmods file anymore.<br/>
2. Change PlayServiceResolver to use ExternalDependencyManager. continue with the same version.<br/>
3. Modify the PackageExporter to use the version from ManifestMod.cs to determine the CommonLib Library Version.

<br/>
<br/>
<br/>

## version 1.2.0

Date:                   24th Dec 2019<br/>
Unity3d Version:        Unity3d 2017.4, Unity3d 2018.4<br/>
PlayServiceResolver:    v1.2.121.0<br/>
JsonDotNet:             v2.0.1

**********************************************************************************************************
** Breaking Changes, please do the following before proceed if you are old user for upgrade:<br/>
** Delete following files/folder:<br/>
** - AppxplorePlugin/Common/Scripts
**********************************************************************************************************

**ChangeLogs:**<br/>
1. Modify the EnableObjCModule.cs file to use only pure UnityEditor.iOS.XCode instead of XUPorter.<br/>
2. Add a MainThreadDispatcher under the namespace Appxplore.Common so it can be use in MultiThreading Environment to avoid Call from MainThread Error.<br/>
3. Drop Support on Unity3d 5.6 as it is deprecated for more than 1 year.<br/>
4. Putting namespace on the Common Scripts, and restructuring them to make it easier to find.<br/>
5. Add a function to reflush all the proguard user rules. In order to do this, please keep in mind to keep your own game set of rules in a file with extension ".pgrules".

<br/>
<br/>
<br/>

## version 1.1.1

Date:                   4th Jul 2019<br/>
Unity3d Version:        5.6.7f2, Unity3d 2017.4, Unity3d 2018.4<br/>
PlayServiceResolver:    v1.2.121.0<br/>
JsonDotNet:             v2.0.1

**ChangeLogs:**<br/>
1. ManifestModEditor - Fix ClearLog function which doesn't work in Unity3d 2017.4 due to different assembly<br/>
2. ManifestMOd - Fix RemoveMetaData function when application is not JObject.<br/>
3. Upgrade PlayServiceResolver.<br/>
4. GradleMod - Modify to let you delete mainTemplate.gradle if you are in Unity3d 2017.4 and later.<br/>
5. GradleMod - Modify to let you know you should tick custom Gradle Template in the popup, but not clear if you didn't read.<br/>
6. GradleMod - Modify to enable minify and use proguard when you are using gradle.

<br/>
<br/>
<br/>

## version: 1.1.0

Date:                   25th Feb 2019<br/>
Unity3d Version:        5.6.6f2<br/>
PlayServiceResolver:    v1.2.99.0<br/>
JsonDotNet:             v2.0.1

**********************************************************************************************************
** Breaking Changes, please do the following before proceed if you are old user for upgrade:<br/>
** Delete following files/folder:<br/>
** - AppxplorePlugin/Common/Editor/ManifestMod<br/>
** - PlayServiceResolver
**********************************************************************************************************

**Change Logs:**<br/>
1. Remove the old ManifestMod file and use the new Json to XML file methods to handle the manifestrules.<br/>
2. New manifestrules json format.<br/>
3. Upgrade PlayServiceResolver.

