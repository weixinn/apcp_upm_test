#import <Foundation/Foundation.h>

// Converts C style string to NSString
static NSString* CreateNSString (const char* string)
{
	if (string)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}

// Converts const char* array to NSString Array
static NSArray* CreateNSArray (const char* arrayOfStr[], int length)
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    for(int i = 0; i < length; i++)
    {
        [array addObject:CreateNSString(arrayOfStr[i])];
    }
    
    return array;
}


// Helper method to create C string copy
static char* MakeStringCopy (const char* string)
{
	if (string == NULL)
		return NULL;
	
	char* res = (char*)malloc(strlen(string) + 1);
	strcpy(res, string);
	return res;
}

// Converts NSString to C style string by way of copy (Mono will free it)
#define MakeStringFromCopy( _x_ ) ( _x_ != NULL && [_x_ isKindOfClass:[NSString class]] ) ? strdup( [_x_ UTF8String] ) : NULL
