﻿using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;

namespace Appxplore.Security
{
    public interface IAS
    {
        byte[] Encrypt(string data);
        bool TryDecrypt(byte[] cipherText, out string outputText);

        string EncryptS(string data);
        bool TryDecryptS(string cipherText, out string outputText);
    }


    /// <summary>
    /// AES Security class used for Encrypt and Descrypt Data
    /// </summary>
    public class ISec : IAS
    {
        const int Iterations = 1000;

        private AesManaged mAes;
        private string mPassword;

        //put here to confuser hacker only
        private readonly string salt = "0c171bd2-923d-416f-a126-7e0c64727830";

        public ISec(string password)
        {
            //only check during Editor since useless after compiled.
#if UNITY_EDITOR
            if (password.Length < 10)
            {
                throw new Exception("password Length should be more than 10");
            }
#endif

            mAes = new AesManaged();
            mAes.BlockSize = 128;
            mAes.KeySize = 128;
            //mAes.Padding = PaddingMode.None;
            mAes.Mode = CipherMode.CBC;

            StringBuilder builder = new StringBuilder();
            builder.Append(password.Substring(0, 3));
            builder.Append(salt.Substring(2, 2));
            builder.Append(password.Substring(4, 6));
            builder.Append(salt.Substring(8, 3));
            mPassword = password;
        }

        public byte[] Encrypt(string data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            // generate a random IV will be used a salt value for generating key
            mAes.GenerateIV();
            //Debug.Log ("IV: " + Convert.ToBase64String(mAes.IV));
            // use derive bytes to generate a key from the password and IV
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(mPassword, mAes.IV, Iterations);

            // generate a key from the password provided
            byte[] key = rfc2898DeriveBytes.GetBytes(16);

            //Debug.Log ("key : " + Convert.ToBase64String(key));

            // encrypt the plainText
            using (var memoryStream = new MemoryStream())
            using (var cryptoStream = new CryptoStream(memoryStream, mAes.CreateEncryptor(key, mAes.IV), CryptoStreamMode.Write))
            {
                // write the salt first not encrypted
                memoryStream.Write(mAes.IV, 0, mAes.IV.Length);

                // convert the plain text string into a byte array
                byte[] bytes = Encoding.UTF8.GetBytes(data);

                // write the bytes into the crypto stream so that they are encrypted bytes
                cryptoStream.Write(bytes, 0, bytes.Length);
                cryptoStream.FlushFinalBlock();

                return memoryStream.ToArray();
            }
        }

        public bool TryDecrypt(byte[] cipherBytes, out string outputText)
        {
            // its pointless trying to decrypt if the cipher text
            // or password has not been supplied
            if (cipherBytes == null)
            {
                outputText = string.Empty;
                return false;
            }

            try
            {
                using (var memoryStream = new MemoryStream(cipherBytes))
                {
                    // get the IV
                    byte[] iv = new byte[16];
                    memoryStream.Read(iv, 0, iv.Length);

                    // use derive bytes to generate key from password and IV
                    var rfc2898DeriveBytes = new Rfc2898DeriveBytes(mPassword, iv, Iterations);

                    byte[] key = rfc2898DeriveBytes.GetBytes(16);

                    using (var cryptoStream = new CryptoStream(memoryStream, mAes.CreateDecryptor(key, iv), CryptoStreamMode.Read))
                    using (var streamReader = new StreamReader(cryptoStream))
                    {
                        outputText = streamReader.ReadToEnd();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                // TODO: log exception
                Console.WriteLine("TryDecrypt Fail: " + ex.Message);

                outputText = "";
                return false;
            }
        }

        public string EncryptS(string data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            return Convert.ToBase64String(Encrypt(data));

        }

        public bool TryDecryptS(string cipherText, out string outputText)
        {
            // its pointless trying to decrypt if the cipher text
            // or password has not been supplied
            if (string.IsNullOrEmpty(cipherText))
            {
                outputText = string.Empty;
                return false;
            }

            try
            {
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                return TryDecrypt(cipherBytes, out outputText);
            }
            catch (Exception ex)
            {
                // TODO: log exception
                Console.WriteLine("TryDecryptS Fail: " + ex.Message);

                outputText = "";
                return false;
            }
        }
    }
}


