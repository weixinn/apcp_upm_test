﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Appxplore.Common
{
    public class MainThreadDispatcher : MonoBehaviour
    {
        private static MainThreadDispatcher m_instance;
        private static readonly Queue<Action> m_eventQueue = new Queue<Action>();
        public static MainThreadDispatcher Instance
        {
            get
            {
                CreateInstanceIfNeeded();
                return m_instance;
                
            }
        }

        public static void CreateInstanceIfNeeded()
        {
            if (m_instance == null)
            {
                GameObject _go = GameObject.Find("Plugins");
                if (_go == null)
                    _go = new GameObject("Plugins");

                m_instance = _go.AddComponent<MainThreadDispatcher>();
            }
        }

        
        private void Awake()
        {
            if (m_instance == null) {
                m_instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        public void enqueue(Action action)
        {
            lock (m_eventQueue)
            {
                m_eventQueue.Enqueue(action);
            }
        }

        public void enqueue(IEnumerator action)
        {
            lock (m_eventQueue)
            {
                m_eventQueue.Enqueue(()=>
                {
                    StartCoroutine(action);
                });
            }
        }


        // Update is called once per frame
        void Update()
        {
            lock (m_eventQueue)
            {
                while (m_eventQueue.Count > 0)
                {
                    m_eventQueue.Dequeue().Invoke();
                }
            }
        }
    }
}


