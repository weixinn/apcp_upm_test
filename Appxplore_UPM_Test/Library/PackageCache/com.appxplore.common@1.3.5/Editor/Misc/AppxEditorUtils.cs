﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using System.Xml;
using UnityEditor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Appxplore.EditorUtils
{
    public class AppxEditorUtils
    {
        public static void SaveManifest(XmlDocument document, string path)
        {
            FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
            StreamWriter streamWriter = new StreamWriter(fileStream);
            using (XmlTextWriter xmlWriter = new XmlTextWriter(streamWriter))
            {
                xmlWriter.Formatting = System.Xml.Formatting.Indented;
                xmlWriter.Namespaces = true;
                document.Save(xmlWriter);
                xmlWriter.Close();
            }

            streamWriter.Close();
            streamWriter.Dispose();
            fileStream.Close();
            fileStream.Dispose();
        }

        public static bool LoadXml(string path, out XmlDocument document)
        {
            try
            {
                using (XmlTextReader tr = new XmlTextReader(path))
                {
                    tr.Namespaces = true;
                    document = new XmlDocument();
                    document.Load(tr);
                    tr.Close();
                    return true;
                }
            }
            catch (XmlException e)
            {
                document = null;
                Debug.LogError(e.Message);
                Debug.LogError("XML file not found at " + path);
                return false;
            }
        }

        public static bool showConflictEditorPop(string type, object optA, object optB)
        {
            return EditorUtility.DisplayDialog(string.Format("Conflict on {0}", type),
                string.Format("Select From Following:\n\n(Option A)\n==========\n{0}\n\n(Option B)\n==========\n{1}",
                    JsonConvert.SerializeObject(optA, Newtonsoft.Json.Formatting.Indented),
                    JsonConvert.SerializeObject(optB, Newtonsoft.Json.Formatting.Indented)
                    ), 
                  "Option A", 
                  "Option B");
        }

    }
}


