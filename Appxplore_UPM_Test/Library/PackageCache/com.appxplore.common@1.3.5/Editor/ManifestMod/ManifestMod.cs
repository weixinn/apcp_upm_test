﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Appxplore.EditorUtils
{
    public class ManifestMod
    {
        public const string VERSION_CODE = "1.3.1";

        private const string ANDROID_MANIFEST_PATH = "Assets/Plugins/Android/AndroidManifest.xml";
        private const string DEFAULT_RULES_PATH = "Assets/AppxplorePlugin/Common/Editor/ManifestMod/Default.manifestrules.json";
        private const string EXPORT_RULES_PATH = "Assets/AppxplorePlugin/Common/Editor/ManifestMod/exported.manifestrules.json";
        private const string MANIFESTMOD_LOG_PATH = "Assets/AppxplorePlugin/Common/Editor/ManifestMod/play_service_mod.json";
        private const string PLUGIN_FOLDER = "/Plugins/Android";
        private static readonly Dictionary<string, object> GMS_META_DATA = new Dictionary<string, object>() { 
            { "@android:name", "com.google.android.gms.version" },
            { "@android:value", "@integer/google_play_services_version" }
        };

        private enum ResultType
        {
            SUCCESS,
            EXISTS,
            KEEP,
            CHANGE,
            MANIFEST_MALFORM
        };

        private enum ManifestType
        {
            ACTIVITY,
            METADATA,
            RECEIVER,
            PERMISSION
        };

        protected static Dictionary<string,object> mainManifest;

        public static void GenOrUpdateManifest()
        {
            if (LoadManifest())
            { 
                UpdateManifest();
                SaveAndroidManifest();

                AssetDatabase.Refresh();
                EditorUtility.DisplayDialog("Manifest Generated/Updated!", "Main AndroidManifest.xml is generated/updated! \nYou may find it at Assets->Plugins->Android->AndroidManifest.xml", "OK");
            }
            else
            {
                Debug.LogError("Failed to generate manifest!");
            }
        }

        public static void ExportManifest()
        {
            if (LoadManifest())
            {
                string jsonContent = JsonConvert.SerializeObject(mainManifest, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(EXPORT_RULES_PATH, jsonContent, System.Text.Encoding.UTF8);
                LogInfo("Export Manifest to Path: " + EXPORT_RULES_PATH);
                EditorUtility.DisplayDialog("Export Android Manifest", "Success!\nExport Manifest to Path: " + EXPORT_RULES_PATH, "OK");
            }
            else
            {
                EditorUtility.DisplayDialog("Export Android Manifest", "Export Manifest Fail!", "OK");
            }
        }

        public static bool InsertMetaData(Dictionary<string,object> mainManifestDict, Dictionary<string,object> metaDataDetail)
        {
            if (!mainManifestDict.ContainsKey("manifest"))
            {
                LogResult(ManifestType.METADATA, ResultType.MANIFEST_MALFORM, metaDataDetail["@android:name"].ToString());
                return false;
            }

            if (((JObject)mainManifestDict["manifest"])["application"] == null)
            {
                LogResult(ManifestType.METADATA, ResultType.MANIFEST_MALFORM, metaDataDetail["@android:name"].ToString());
                return false;
            }

            var app = (JObject)((JObject)mainManifestDict["manifest"])["application"];

            if (app["meta-data"] == null)
            {
                app.Add("meta-data", JObject.FromObject(metaDataDetail));
                LogResult(ManifestType.METADATA, ResultType.SUCCESS, metaDataDetail["@android:name"].ToString());
                return true;
            }
            else
            {
                bool result = false;
                List<Dictionary<string, object>> metadatas = null;
                if (app["meta-data"].GetType() == typeof(JArray))
                    metadatas = app["meta-data"].ToObject<List<Dictionary<string, object>>>();
                else
                {
                    metadatas = new List<Dictionary<string, object>>();
                    metadatas.Add(app["meta-data"].ToObject<Dictionary<string, object>>());
                }
                app["meta-data"].Parent.Remove();

                int indexFind = metadatas.FindIndex(ni => ni["@android:name"].Equals(metaDataDetail["@android:name"]));

                if (indexFind == -1)
                {
                    metadatas.Add(metaDataDetail);
                    LogResult(ManifestType.METADATA, ResultType.SUCCESS, metaDataDetail["@android:name"].ToString());
                    result = true;
                }
                else
                {
                    if (!metadatas[indexFind]["@android:value"].Equals(metaDataDetail["@android:value"].ToString()))
                    {
                        //Conflict, show conflict and let player choose
                        if (AppxEditorUtils.showConflictEditorPop(ManifestType.METADATA.ToString(), metaDataDetail, metadatas[indexFind]))
                        {
                            //want to change to new one.
                            metadatas[indexFind] = metaDataDetail;
                            LogResult(ManifestType.METADATA, ResultType.CHANGE, metaDataDetail["@android:name"].ToString());
                            result = true;
                        }
                        else
                        {
                            LogResult(ManifestType.METADATA, ResultType.KEEP, metadatas[indexFind]["@android:name"].ToString());
                        }

                    }
                    else
                    {
                        LogResult(ManifestType.METADATA, ResultType.EXISTS, metaDataDetail["@android:name"].ToString());
                    }
                }
                app.Add("meta-data", JArray.FromObject(metadatas));
                return result;
            }
        }


        public static void InsertActivity(Dictionary<string, object> activityDetail)
        {
            if (mainManifest != null)
                InsertActivity(mainManifest, activityDetail);
            else
                LogError("LoadManifest first before Running any action!");
        }

        public static void InsertActivity(Dictionary<string, object> mainManifestDict, Dictionary<string, object> activityDetail)
        {
            if (!mainManifestDict.ContainsKey("manifest"))
            {
                LogResult(ManifestType.ACTIVITY, ResultType.MANIFEST_MALFORM, activityDetail["@android:name"].ToString());
                return;
            }

            if (((JObject)mainManifestDict["manifest"])["application"] == null)
            {
                LogResult(ManifestType.ACTIVITY, ResultType.MANIFEST_MALFORM, activityDetail["@android:name"].ToString());
                return;
            }

            var app = (JObject)((JObject)mainManifestDict["manifest"])["application"];

            if (app["activity"] == null)
            {
                app.Add("activity", JObject.FromObject(activityDetail));
                LogResult(ManifestType.ACTIVITY, ResultType.SUCCESS, activityDetail["@android:name"].ToString());
            }
            else
            {
                List<Dictionary<string, object>> activities = null;
                if (app["activity"].GetType() == typeof(JArray))
                    activities = app["activity"].ToObject<List<Dictionary<string, object>>>();
                else
                {
                    activities = new List<Dictionary<string, object>>();
                    activities.Add(app["activity"].ToObject<Dictionary<string, object>>());
                }
                app["activity"].Parent.Remove();

                int indexFind = activities.FindIndex(ni => ni["@android:name"].Equals(activityDetail["@android:name"]));

                if (indexFind == -1)
                {
                    activities.Add(activityDetail);
                    LogResult(ManifestType.ACTIVITY, ResultType.SUCCESS, activityDetail["@android:name"].ToString());
                }
                else
                {
                    if (!JsonConvert.SerializeObject(activities[indexFind]).Equals(JsonConvert.SerializeObject(activityDetail)))
                    {
                        //Conflict, show conflict and let player choose
                        if (AppxEditorUtils.showConflictEditorPop(ManifestType.ACTIVITY.ToString(), activityDetail, activities[indexFind]))
                        {
                            //want to change to new one.
                            activities[indexFind] = activityDetail;
                            LogResult(ManifestType.ACTIVITY, ResultType.CHANGE, activityDetail["@android:name"].ToString());
                        }
                        else
                        {
                            LogResult(ManifestType.ACTIVITY, ResultType.KEEP, activities[indexFind]["@android:name"].ToString());
                        }
                    }
                    else
                    {
                        LogResult(ManifestType.ACTIVITY, ResultType.EXISTS, activityDetail["@android:name"].ToString());
                    }
                }
                app.Add("activity", JArray.FromObject(activities));
            }
        }

        public static void InsertReceiver(Dictionary<string, object> receiverDetail)
        {
            if (mainManifest != null)
                InsertReceiver(mainManifest, receiverDetail);
            else
                LogError("LoadManifest first before Running any action!");
        }

        public static void InsertReceiver(Dictionary<string, object> mainManifestDict, Dictionary<string, object> receiverDetail)
        {
            if (!mainManifestDict.ContainsKey("manifest"))
            {
                LogResult(ManifestType.RECEIVER, ResultType.MANIFEST_MALFORM, receiverDetail["@android:name"].ToString());
                return;
            }

            if (((JObject)mainManifestDict["manifest"])["application"] == null)
            {
                LogResult(ManifestType.RECEIVER, ResultType.MANIFEST_MALFORM, receiverDetail["@android:name"].ToString());
                return;
            }

            var app = (JObject)((JObject)mainManifestDict["manifest"])["application"];

            if (app["receiver"] == null)
            {
                app.Add("receiver", JObject.FromObject(receiverDetail));
                LogResult(ManifestType.RECEIVER, ResultType.SUCCESS, receiverDetail["@android:name"].ToString());
            }
            else
            {
                List<Dictionary<string, object>> receivers = null;
                if (app["receiver"].GetType() == typeof(JArray))
                    receivers = app["receiver"].ToObject<List<Dictionary<string, object>>>();
                else
                {
                    receivers = new List<Dictionary<string, object>>();
                    receivers.Add(app["receiver"].ToObject<Dictionary<string, object>>());
                }
                app["receiver"].Parent.Remove();

                int indexFind = receivers.FindIndex(ni => ni["@android:name"].Equals(receiverDetail["@android:name"]));

                if (indexFind == -1)
                {
                    receivers.Add(receiverDetail);
                    LogResult(ManifestType.RECEIVER, ResultType.SUCCESS, receiverDetail["@android:name"].ToString());
                }
                else
                {
                    if (!JsonConvert.SerializeObject(receivers[indexFind]).Equals(JsonConvert.SerializeObject(receiverDetail)))
                    {
                        //Conflict, show conflict and let player choose
                        if (AppxEditorUtils.showConflictEditorPop(ManifestType.RECEIVER.ToString(), receiverDetail, receivers[indexFind]))
                        {
                            //want to change to new one.
                            receivers[indexFind] = receiverDetail;
                            LogResult(ManifestType.RECEIVER, ResultType.CHANGE, receiverDetail["@android:name"].ToString());
                        }
                        else
                        {
                            LogResult(ManifestType.RECEIVER, ResultType.KEEP, receivers[indexFind]["@android:name"].ToString());
                        }
                    }
                    else
                    {
                        LogResult(ManifestType.RECEIVER, ResultType.EXISTS, receiverDetail["@android:name"].ToString());
                    }
                }
                app.Add("receiver", JArray.FromObject(receivers));
            }
        }

        public static void InsertUsesPermissions(Dictionary<string, object> permissionDataDetail)
        {
            if (mainManifest != null)
                InsertUsesPermissions(mainManifest, permissionDataDetail);
            else
                LogError("LoadManifest first before Running any action!");
        }

        public static void InsertUsesPermissions(Dictionary<string, object> mainManifestDict, Dictionary<string, object> permissionDataDetail)
        {
            if (!mainManifestDict.ContainsKey("manifest"))
            {
                LogResult(ManifestType.PERMISSION, ResultType.MANIFEST_MALFORM, permissionDataDetail["@android:name"].ToString());
                return;
            }

            var manifest = (JObject)mainManifestDict["manifest"];

            if (manifest["uses-permission"] == null)
            {
                manifest.Add("uses-permission", JObject.FromObject(permissionDataDetail));
                LogResult(ManifestType.PERMISSION, ResultType.SUCCESS, permissionDataDetail["@android:name"].ToString());
            }
            else
            {
                List<Dictionary<string, object>> permissions = null;
                if (manifest["uses-permission"].GetType() == typeof(JArray))
                    permissions = manifest["uses-permission"].ToObject<List<Dictionary<string, object>>>();
                else
                {
                    permissions = new List<Dictionary<string, object>>();
                    permissions.Add(manifest["uses-permission"].ToObject<Dictionary<string, object>>());
                }
                manifest["uses-permission"].Parent.Remove();

                int indexFind = permissions.FindIndex(ni => ni["@android:name"].Equals(permissionDataDetail["@android:name"]));

                if (indexFind == -1)
                {
                    permissions.Add(permissionDataDetail);
                    LogResult(ManifestType.PERMISSION, ResultType.SUCCESS, permissionDataDetail["@android:name"].ToString());
                }
                else
                {
                    if (!JsonConvert.SerializeObject(permissions[indexFind]).Equals(JsonConvert.SerializeObject(permissionDataDetail)))
                    {
                        //Conflict, show conflict and let player choose
                        if (AppxEditorUtils.showConflictEditorPop(ManifestType.PERMISSION.ToString(), permissionDataDetail, permissions[indexFind]))
                        {
                            //want to change to new one.
                            permissions[indexFind] = permissionDataDetail;
                            LogResult(ManifestType.PERMISSION, ResultType.CHANGE, permissionDataDetail["@android:name"].ToString());
                        }
                        else
                        {
                            LogResult(ManifestType.PERMISSION, ResultType.KEEP, permissions[indexFind]["@android:name"].ToString());
                        }
                    }
                    else
                    {
                        LogResult(ManifestType.PERMISSION, ResultType.EXISTS, permissionDataDetail["@android:name"].ToString());
                    }
                }
                manifest.Add("uses-permission", JArray.FromObject(permissions));
            }
        }

        public static bool RemoveMetaData(string _androidName)
        {
            if (mainManifest != null)
                return RemoveMetaData(mainManifest, _androidName);
            else
            {
                LogError("LoadManifest first before Running any action!");
                return false;
            }
        }

        public static bool RemoveMetaData(Dictionary<string, object> mainManifestDict, string _androidName)
        {
            if (!mainManifestDict.ContainsKey("manifest"))
                return false;

            if (((JObject)mainManifestDict["manifest"])["application"] == null)
                return false;

            if (((JObject)mainManifestDict["manifest"])["application"].GetType() != typeof(JObject))
                return false;

            var app = (JObject)((JObject)mainManifestDict["manifest"])["application"];

            if (app["meta-data"] == null)
                return false;
            else
            {
                List<Dictionary<string, object>> metadatas = null;
                if (app["meta-data"].GetType() == typeof(JArray))
                    metadatas = app["meta-data"].ToObject<List<Dictionary<string, object>>>();
                else
                {
                    metadatas = new List<Dictionary<string, object>>();
                    metadatas.Add(app["meta-data"].ToObject<Dictionary<string, object>>());
                }

                int indexFind = metadatas.FindIndex(ni => ni["@android:name"].Equals(_androidName));
                if (indexFind != -1)
                {
                    app["meta-data"].Parent.Remove();
                    metadatas.RemoveAt(indexFind);
                    app.Add("meta-data", JArray.FromObject(metadatas));
                    return true;
                }
                else
                    return false;
            }
        }

        public static string EnableGradleModAndroidManifest()
        {
            string result = "- Remove GMS Meta-Data Tag:\n";
            List<string> changedPath = removeAllGMSVersion(true);
            if (changedPath.Count > 0)
            {
                RecordPaths(changedPath);
                foreach (string path in changedPath)
                    result += string.Format("\t* {0}\n", path);
            }
            else
            {
                result += "\t *No File has changed!\n";
            }

            if (addGMSVersion(Application.dataPath + PLUGIN_FOLDER + "/AndroidManifest.xml"))
                result += "- added GMS Version in AndroidManifest.xml";

            return result;
        }

        public static string DisableGradleModAndroidManifest()
        {
            string data = string.Empty;
            string result = "- Remove GMS Meta-Data Tag:\n";

            if (File.Exists(MANIFESTMOD_LOG_PATH))
                data = File.ReadAllText(MANIFESTMOD_LOG_PATH);

            if (string.IsNullOrEmpty(data))
            {
                //empty, show message and quit
                result += "\t* No File has changed!\n";
            }
            else
            {
                List<string> changedPaths = JsonConvert.DeserializeObject<List<string>>(data);
                foreach (string path in changedPaths)
                {
                    if (!string.IsNullOrEmpty(path))
                    {
                        if (addGMSVersion(Application.dataPath + path))
                            result += string.Format("\t* {0}\n", path);
                    }
                }
            }

            AssetDatabase.DeleteAsset(MANIFESTMOD_LOG_PATH);
            result += "- Clear Up AndroidManifest Change record!\n";
            return result;
        }

        public static bool LoadManifest()
        {
            if (!File.Exists(ANDROID_MANIFEST_PATH))
            {
                CreateDefaultManifest();
            }

            return LoadManifest(ANDROID_MANIFEST_PATH, out mainManifest);
        }

        public static bool LoadManifest(string _path, out Dictionary<string,object> outputDict)
        {
            outputDict = null;
            try
            {
                XmlDocument doc = null;
                if (AppxEditorUtils.LoadXml(_path, out doc))
                {
                    outputDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeXmlNode(doc));
                    LogInfo("AndroidManifest Loaded: " + _path);
                    return true;
                }
                else 
                    throw new SystemException("Load XML Fail!");
            }
            catch (SystemException e)
            {
                LogError(e.Message);
                return false;
            }
        }

        public static void updateOneRuleFiles(string rulePath)
        {
            if (!rulePath.Equals(DEFAULT_RULES_PATH))
            {
                string json = File.ReadAllText(rulePath);
                if (!string.IsNullOrEmpty(json))
                {
                    Dictionary<string, object> ruleDic = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    if (ruleDic.ContainsKey("meta-data"))
                    {
                        List<Dictionary<string, object>> metadatas = ((JArray)ruleDic["meta-data"]).ToObject<List<Dictionary<string, object>>>();
                        foreach (Dictionary<string, object> metadata in metadatas)
                        {
                            InsertMetaData(mainManifest, metadata);
                        }
                    }

                    if (ruleDic.ContainsKey("activity"))
                    {
                        List<Dictionary<string, object>> activities = ((JArray)ruleDic["activity"]).ToObject<List<Dictionary<string, object>>>();
                        foreach (Dictionary<string, object> activity in activities)
                        {
                            InsertActivity(mainManifest, activity);
                        }
                    }

                    if (ruleDic.ContainsKey("receiver"))
                    {
                        List<Dictionary<string, object>> receivers = ((JArray)ruleDic["receiver"]).ToObject<List<Dictionary<string, object>>>();
                        foreach (Dictionary<string, object> receiver in receivers)
                        {
                            InsertReceiver(mainManifest, receiver);
                        }
                    }

                    if (ruleDic.ContainsKey("uses-permission"))
                    {
                        List<Dictionary<string, object>> permissions = ((JArray)ruleDic["uses-permission"]).ToObject<List<Dictionary<string, object>>>();
                        foreach (Dictionary<string, object> permission in permissions)
                        {
                            InsertUsesPermissions(mainManifest, permission);
                        }
                    }

                    SaveAndroidManifest(mainManifest, ANDROID_MANIFEST_PATH);
                }
            }
        }

        public static bool SaveAndroidManifest()
        {
            return SaveAndroidManifest(mainManifest, ANDROID_MANIFEST_PATH);
        }

        public static bool SaveAndroidManifest(Dictionary<string, object> mainManifestDict, string path)
        {
            try
            {
                XmlDocument doc = JsonConvert.DeserializeXmlNode(JsonConvert.SerializeObject(mainManifestDict));
                AppxEditorUtils.SaveManifest(doc, path);
                return true;
            }
            catch (System.Exception e)
            {
                LogError("SaveAndroidManifest Error, Check Following Exception Message:");
                Debug.LogException(e);
                return false;
            }
        }

        public static void UpdateManifest()
        {
            string[] rulesPathList = Directory.GetFiles(Application.dataPath, "*.manifestrules.json", SearchOption.AllDirectories);
            foreach (string path in rulesPathList)
            {
                updateOneRuleFiles(path);
            }
        }

        #region Internal Functions
        private static bool CreateDefaultManifest()
        {
            if (!File.Exists(DEFAULT_RULES_PATH))
            {
                Debug.LogError("Template manifestrules file is not found!");
                return false;
            }

            string jsonRules = File.ReadAllText(DEFAULT_RULES_PATH);
            if (string.IsNullOrEmpty(jsonRules))
            {
                Debug.LogError("Template manifestrules file is Empty!");
                return false;
            }

            XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonRules);
            AppxEditorUtils.SaveManifest(doc, ANDROID_MANIFEST_PATH);
            LogInfo("Create Default AndroidManifest...");
            return true;
        }

        private static bool addGMSVersion(string path)
        {
            XmlDocument doc = null;
            if (AppxEditorUtils.LoadXml(path, out doc))
            {
                Dictionary<string, object> androidManifest = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeXmlNode(doc));

                if (InsertMetaData(androidManifest, GMS_META_DATA))
                {
                    SaveAndroidManifest(androidManifest, path);
                    return true;
                }
            }

            return false;
        }

        private static bool removeGMSVersion(string path)
        {
            XmlDocument doc = null;
            if (AppxEditorUtils.LoadXml(path, out doc))
            {
                Dictionary<string, object> androidManifest = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeXmlNode(doc));
                if (RemoveMetaData(androidManifest, "com.google.android.gms.version"))
                {
                    SaveAndroidManifest(androidManifest, path);
                    return true;
                }
            }

            return false;
        }

        private static List<string> removeAllGMSVersion(bool _exceptMain)
        {
            string[] paths = Directory.GetFiles(Application.dataPath + PLUGIN_FOLDER, "AndroidManifest.xml", SearchOption.AllDirectories);
            List<string> changedPath = new List<string>();
            foreach (string path in paths)
            {
                if (_exceptMain && path.Contains(Application.dataPath + PLUGIN_FOLDER + "/AndroidManifest.xml"))
                    continue;

                if (removeGMSVersion(path))
                    changedPath.Add(path.Substring(Application.dataPath.Length));
            }

            LogInfo("Remove GMSVersion In Paths:\n" + string.Join("\n", changedPath.ToArray()) + "\n");
            return changedPath;
        }

        private static void RecordPaths(List<string> paths)
        {
            string record = string.Empty;

            if (File.Exists(MANIFESTMOD_LOG_PATH))
                record = File.ReadAllText(MANIFESTMOD_LOG_PATH);
            List<string> filePaths = JsonConvert.DeserializeObject<List<string>>(record);
            if (filePaths == null)
                filePaths = new List<string>();

            for (int i = 0; i < paths.Count; i++)
            {
                if (!filePaths.Contains(paths[i]))
                    filePaths.Add(paths[i]);
            }
            File.WriteAllText(MANIFESTMOD_LOG_PATH, JsonConvert.SerializeObject(filePaths));
        }
        #endregion

        #region LOGGING
        private static void LogResult(ManifestType _type, ResultType _result, string _name)
        {
            Debug.Log(string.Format("[ManifestMod] - Insert {0} \"{1}\" : {2}", _type.ToString(), _result.ToString(), _name));
        }

        private static void LogInfo(string _message)
        {
            Debug.Log(string.Format("[ManifestMod] - {0}", _message));
        }

        private static void LogError(string _message)
        {
            Debug.LogError(string.Format("[ManifestMod] - {0}", _message));
        }
        #endregion
    }
}