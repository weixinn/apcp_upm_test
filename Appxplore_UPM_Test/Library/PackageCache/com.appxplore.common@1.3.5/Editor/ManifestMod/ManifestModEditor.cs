﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using Newtonsoft.Json;
using System.IO;

namespace Appxplore.EditorUtils
{
    [InitializeOnLoad]
    public class ManifestModEditor 
    {
        [MenuItem("AppxplorePlugin/ManifestMod/Generate OR Update Manifest", false, 1)]
        public static void GenerateManifest()
        {
            ClearLog();
            ManifestMod.GenOrUpdateManifest();
            AssetDatabase.Refresh();
        }

        [MenuItem("AppxplorePlugin/ManifestMod/Export Current Manifest", false, 1)]
        public static void ExportManifest()
        {
            ClearLog();
            ManifestMod.ExportManifest();
        }

        public static void ClearLog()
        {
#if UNITY_2017_4_OR_NEWER
            var assembly = Assembly.GetAssembly(typeof(UnityEditor.Editor));
            var type = assembly.GetType("UnityEditor.LogEntries");
            var method = type.GetMethod("Clear");
            method.Invoke(new object(), null);
#else
            var assembly = Assembly.GetAssembly(typeof(UnityEditor.ActiveEditorTracker));
            var type = assembly.GetType("UnityEditorInternal.LogEntries");
            var method = type.GetMethod("Clear");
            method.Invoke(new object(), null);
#endif
        }
    }
}
