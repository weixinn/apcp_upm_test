﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

public class EnableObjCModule {
	
	[PostProcessBuild(1500)]
	public static void OnPostProcessBuild(BuildTarget target, string path)
	{
#if UNITY_IOS
        if(target != BuildTarget.iOS)
        {
            return;
        }

        string _projectPath = PBXProject.GetPBXProjectPath(path);

        PBXProject _project = new PBXProject();
        _project.ReadFromFile(_projectPath);

        string _targetId = _project.TargetGuidByName("Unity-iPhone");
        _project.SetBuildProperty(_targetId, "CLANG_ENABLE_MODULES", "YES");

        _project.WriteToFile(_projectPath);
#endif
	}
	
}
